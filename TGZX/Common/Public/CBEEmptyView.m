//
//  CBEEmptyView.m
//  CBECommerce
//
//  Created by Eleven_Liu on 2020/5/18.
//  Copyright © 2020 Eleven_Liu. All rights reserved.
//

#import "CBEEmptyView.h"

@implementation CBEEmptyView

+ (instancetype)CBENoDataEmptyView {
    
    return [self CBENoDataEmptyViewWithImgName:@"empty_img" text:@"暂无数据"];
}

+ (instancetype)CBENoDataEmptyViewWithImgName:(NSString *)imgName text:(NSString *)text
{
    CBEEmptyView *diy = [CBEEmptyView emptyViewWithImage:IMAGE_NAME(imgName) titleStr:text detailStr:nil];
    diy.titleLabFont = FONT_SIZE(14);
    diy.titleLabTextColor = RGB(136, 136, 136);
    return diy;
}

+ (instancetype)CBENoNetworkEmptyWithTarget:(id)target action:(SEL)action {
    
    CBEEmptyView *diy = [CBEEmptyView emptyActionViewWithImage:IMAGE_NAME(@"net_break")
                                                    titleStr:@"您的网络不太给力，请稍后重试"
                                                   detailStr:nil
                                                 btnTitleStr:@"点击刷新"
                                                      target:target
                                                      action:action];
    diy.titleLabFont = FONT_SIZE(12);
    diy.titleLabTextColor = RGB(136, 136, 136);
    
    return diy;
}

@end
