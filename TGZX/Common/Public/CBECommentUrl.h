//
//  CBECommentUrl.h
//  CBECommerce
//
//  Created by jifan on 2020/5/19.
//  Copyright © 2020 Eleven_Liu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBECommentUrl : NSObject


#pragma mark - ----------------- IP

//服务器地址标识  1：生产   2：测试
#define JFServerEnvironment 2



FOUNDATION_EXPORT NSString *const HostIp;


#pragma mark - ----------------- Mine

//收货地址列表
FOUNDATION_EXPORT NSString * const MineAddressList;
//删除收货地址
FOUNDATION_EXPORT NSString * const DeleteAddressList;
//添加收货地址
FOUNDATION_EXPORT NSString * const AddAddressList;
//修改收货地址
FOUNDATION_EXPORT NSString * const ModifyAddressList;

////海关信息
//新增、编辑海关信息
FOUNDATION_EXPORT NSString * const AddModifyCuctomsInfo;
//获取海关信息
FOUNDATION_EXPORT NSString * const GetCuctomsInfo;


#pragma mark - -----------------  login

//注册获取验证码
FOUNDATION_EXPORT NSString * const GetRegisterCode;
//登录
FOUNDATION_EXPORT NSString * const LoginApi;
//绑定手机号
FOUNDATION_EXPORT NSString * const BindMoileApi;


#pragma mark - ----------------- 分类

//展示商品所有分类
FOUNDATION_EXPORT NSString * const api_show_goods_category;
//展示商品子级所有分类
FOUNDATION_EXPORT NSString * const api_subclass_goods_category;
//展示商品信息
FOUNDATION_EXPORT NSString * const api_show_goods;
//商品全部评论
FOUNDATION_EXPORT NSString * const api_all_comment;
//商品详情
FOUNDATION_EXPORT NSString * const api_show_goods_info;

#pragma mark - ----------------- 订单

//商品详情
FOUNDATION_EXPORT NSString * const api_confirm_order;

@end

NS_ASSUME_NONNULL_END
