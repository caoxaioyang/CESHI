//
//  CBELoadHtmlVC.m
//  CBECommerce
//
//  Created by Eleven_Liu on 2020/5/26.
//  Copyright © 2020 Eleven_Liu. All rights reserved.
//

#import "CBELoadHtmlVC.h"

@interface CBELoadHtmlVC ()

@end

@implementation CBELoadHtmlVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


#pragma mark - ----------------- webView Delegate

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    
    WKNavigationActionPolicy policy = WKNavigationActionPolicyAllow;
//    NSURL    *url  = navigationAction.request.URL;
//    NSString *host =  url.host;
//    NSString *path =  url.path;
//    NSString *queryStr = [url query];
//    NSString *absolute = [url absoluteString];
    
    decisionHandler(policy);
}



@end
