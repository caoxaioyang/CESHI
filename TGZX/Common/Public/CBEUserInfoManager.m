//
//  CBEUserInfoManager.m
//  CBECommerce
//
//  Created by jifan on 2020/5/19.
//  Copyright © 2020 Eleven_Liu. All rights reserved.
//

#import "CBEUserInfoManager.h"

#define FILEPATCH [DocumentPath stringByAppendingPathComponent:@"userInfo.data"]

@interface CBEUserInfoManager ()

@end

@implementation CBEUserInfoManager


#pragma mark - ----------------- 方法实现

+ (instancetype)shared {
    static CBEUserInfoManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[CBEUserInfoManager alloc] init];
    });
    return manager;
}

+ (CBEUserInfoModel *)userInfo {
    CBEUserInfoModel *userInfo  = [NSKeyedUnarchiver unarchiveObjectWithFile:FILEPATCH];
    return userInfo;
}

//更新数据
+ (void)updateUserInfo {
    [[CBEUserInfoManager shared] updateUserInfo];
}

-(void)updateUserInfo{
    if (!EmptyObj(self.userInfoModel)) {
        BOOL successOrNot = [NSKeyedArchiver archiveRootObject:self.userInfoModel toFile:FILEPATCH];
        NSLog(@"更新数据成功与否：%d", successOrNot);
    }else {
        NSLog(@"更新数据成功与否：失败");
    }
}


+ (BOOL)cleanUserInfo {
    //删除归档文件
    NSFileManager *defaultManager = [NSFileManager defaultManager];
    if ([defaultManager isDeletableFileAtPath:FILEPATCH]) {
        [defaultManager removeItemAtPath:FILEPATCH error:nil];
        return YES;
    }
    return NO;
}

//是否已经登陆
+ (BOOL)hasLogin {
    return [[CBEUserInfoManager shared] hasLogin];
}
- (BOOL)hasLogin {
    CBEUserInfoModel *userInfo = [CBEUserInfoManager userInfo];
    if (userInfo) {
        return YES;
    }else {
        return NO;
    }
}

//某些操作只能登陆，才能使用
+ (BOOL)mustLogin {
     return [[CBEUserInfoManager shared] mustLogin];
}

- (BOOL)mustLogin{
    if ([self hasLogin]) {
        return NO;
    }else{
        [self goToLogin];
        return YES;
    }
}

//退出登录
+ (void)logoutComplish:(void (^)(id, NSError *))complish {
    [[CBEUserInfoManager shared] logoutComplish:complish];
}

- (void)logoutComplish:(void (^)(id, NSError *))complish{
    //后台退出
//    [CBENetworkClass getWithHostIp:HostIp UrlString:JFLogout parameters:@{} complish:^(id  _Nonnull responseObject, NSError * _Nonnull error) {
//        if ([StrFmt(@"%@", responseObject[@"recode"]) isEqualToString:@"1"]) {
//            [self.userInfoCache removeObjectForKey:JFPersonalCenterPersonInfoUrl];
//            self.userInfoModel = nil;
//
//            if ([JFUserInfoManager cleanUserInfo]) {
//                NSLog(@"清除用户归档数据成功");
//            }
//
//            //移除token
//            if ([YYKeychain getPasswordForService:kServiceName account:kAccountName]) {
//                [YYKeychain deletePasswordForService:kServiceName account:kAccountName];
//            }
//            //通知登录状态改变
//            SendNotify(UserLoginStatusChange, nil);
//            //推送,用户退出,别名去掉
//            [self _JPushLogout];
//
//            if (responseObject) complish(responseObject, error);
//        }
//
//        //回来是数据已经清除，这里让它回到root
//        UITabBarController* rootVC = (UITabBarController *)[UIApplication sharedApplication].keyWindow.rootViewController;
//        if (rootVC.presentedViewController) {
//            //为了防止在2级界面以上
//            [rootVC dismissViewControllerAnimated:YES completion:nil];
//        }
//        UINavigationController *nav = rootVC.selectedViewController;
//        [nav popToRootViewControllerAnimated:NO];
//    }];
}

//获得用户信息
+ (void)getUserInfo {
    [[CBEUserInfoManager shared] getUserInfo];
}

- (void)getUserInfo {
//    if ([YYKeychain getPasswordForService:kServiceName account:kAccountName]) {
//        NSString *token = [YYKeychain getPasswordForService:kServiceName account:kAccountName];
//
//    }else {
//        [MBProgressHUD showMessageWithText:@"token不能为空"];
//    }
    
//    [CBENetworkClass getWithHostIp:HostIp UrlString:JFPersonalCenterPersonInfoUrl parameters:@{@"token":token} complish:^(id  _Nonnull responseObject, NSError * _Nonnull error) {
//        [MBProgressHUD hideLoading];
//        NSString *recode = StrFmt(@"%@", responseObject[@"recode"]);
//        if ([recode isEqualToString:@"1"]) {
//            id result = [JFUserInfoModel mj_objectWithKeyValues:responseObject[@"data"]];
//            self.userInfoModel = result;
//            self.userInfoModel.token = token;
//
//            //设置极光推送别名
//            [self JPushSetAliasWithUserId:self.userInfoModel.ID];
//
//            //由于yycache可能丢失数据，归档一份数据
//            [self updateUserInfo];
//
//            if (self.isLogin) [MBProgressHUD showMessageWithText:@"登录成功"];
//
//            //通知登录状态改变
//            SendNotify(UserLoginStatusChange, nil);
//
//        }else if ([recode isEqualToString:@"2"]) {
//            [MBProgressHUD showMessageWithText:responseObject[@"error"]];
//            [JFUserInfoManager logoutComplish:^(id result, NSError * error) {}];
//        }else {
//            [MBProgressHUD showMessageWithText: responseObject[@"error"]];
//        }
//    }];
    
}





#pragma mark - ----------------- other

//跳转登录页
- (void)goToLogin{
//    JFLoginVC *loginVC = [JFLoginVC new];
//    UIViewController *appRootVC = [UIApplication sharedApplication].keyWindow.rootViewController;
//    UIViewController *topVC = appRootVC;
//    if (topVC.presentedViewController) {
//        topVC = topVC.presentedViewController;
//    }
//    JFBaseNVC *nav = [[JFBaseNVC alloc] initWithRootViewController:loginVC];
//    [topVC presentViewController:nav animated:YES completion:nil];
}



@end
