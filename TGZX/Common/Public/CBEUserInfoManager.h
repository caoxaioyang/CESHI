//
//  CBEUserInfoManager.h
//  CBECommerce
//
//  Created by jifan on 2020/5/19.
//  Copyright © 2020 Eleven_Liu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CBEUserInfoModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBEUserInfoManager : NSObject

+ (instancetype)shared;

@property (nonatomic, strong, nullable) CBEUserInfoModel * userInfoModel;

+ (CBEUserInfoModel *)userInfo;

+ (void)updateUserInfo;

+ (BOOL)cleanUserInfo; // 清除数据

+ (BOOL)hasLogin;

+ (BOOL)mustLogin;

+ (void)logoutComplish:(void (^)(id, NSError *))complish;

+ (void)getUserInfo;

@end

NS_ASSUME_NONNULL_END
