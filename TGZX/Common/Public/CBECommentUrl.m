//
//  CBECommentUrl.m
//  CBECommerce
//
//  Created by jifan on 2020/5/19.
//  Copyright © 2020 Eleven_Liu. All rights reserved.
//

#import "CBECommentUrl.h"

@implementation CBECommentUrl

#pragma mark - ----------------- IP
#if (JFServerEnvironment == 1)
NSString * const HostIp = @"";
#elif (JFServerEnvironment == 2)
NSString * const HostIp = @"http://test.haitao.api.dxanm.com/index/";
#endif


#pragma mark - ----------------- Mine

////地址
//收货地址列表
NSString * const MineAddressList = @"user_address/show_address";
//删除收货地址
NSString * const DeleteAddressList = @"user_address/del_address";
//添加收货地址
NSString * const AddAddressList = @"user_address/add_address";
//修改收货地址
NSString * const ModifyAddressList = @"user_address/modify_address";

////海关信息
//获取海关信息
NSString * const GetCuctomsInfo = @"user_customs_info/show_user_customs_info";
//新增、编辑海关信息
NSString * const AddModifyCuctomsInfo = @"user_customs_info/modify_user_customs_info";


#pragma mark - -----------------  login

//注册获取验证码
NSString * const GetRegisterCode = @"login/send_message";
//登录
NSString * const LoginApi = @"login/login";
//绑定手机号
NSString * const BindMoileApi = @"login/binding_number";


#pragma mark - ----------------- 分类

//展示商品所有分类
NSString * const api_show_goods_category = @"goods_category/show_goods_category";
//展示商品子级所有分类
NSString * const api_subclass_goods_category = @"goods_category/subclass_goods_category";
//展示商品信息
NSString * const api_show_goods = @"goods/show_goods";
//商品全部评论
NSString * const api_all_comment = @"index/goods/all_comment";
//商品详情
NSString * const api_show_goods_info = @"goods/show_goods_info";

#pragma mark - ----------------- 订单
//确认订单
NSString * const api_confirm_order = @"order/confirm_order";


@end
