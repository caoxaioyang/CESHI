//
//  CBEMacros.h
//  CBECommerce
//
//  Created by Eleven_Liu on 2020/5/18.
//  Copyright © 2020 Eleven_Liu. All rights reserved.
//

#ifndef CBEMacros_h
#define CBEMacros_h


/* 强引用转换防止循环引用 */
#define kSelfWeak __weak typeof(self) weakSelf = self
#define kSelfStrong __strong __typeof__(weakSelf) strongSelf = weakSelf

#define IMAGE_NAME(x) [UIImage imageNamed:x]
#define StrFmt(format,...) [NSString stringWithFormat:format,##__VA_ARGS__]

//目前是以4.7寸作为标准
#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height
#define kScreenWidthRatio  (SCREEN_WIDTH / 375.0)
#define kScreenHeightRatio (SCREEN_HEIGHT / 667.0)
#define AdaptedWidth(x)  ceilf((x) * kScreenWidthRatio)
#define AdaptedHeight(x) ceilf((x) * kScreenHeightRatio)

#define FONT_SIZE(x) [UIFont fontWithName:@"PingFangSC-Regular" size:(((x) * kScreenWidthRatio))]
#define BOLD_SIZE(x) [UIFont fontWithName:@"PingFang-SC-Bold" size:(((x) * kScreenWidthRatio))]
#define FontWithNameAndSize(NAME,x) [UIFont fontWithName:NAME size:((x) * kScreenWidthRatio)]

#ifdef IOS9
#define MEDIUM_SIZE(x) [UIFont fontWithName:@"PingFangSC-Medium" size:(((x) * kScreenWidthRatio))]
#else
#define MEDIUM_SIZE(x) [UIFont fontWithName:@"PingFangSC-Regular" size:(((x) * kScreenWidthRatio))]
#endif

//弱指针
#define WeakSelf(weakSelf)  __weak __typeof(&*self)weakSelf = self;

//判断版本
#define IOS7 ([[UIDevice currentDevice].systemVersion doubleValue]>=7.0)
#define IOS8 ([[UIDevice currentDevice].systemVersion doubleValue]>=8.0)
#define IOS9 ([[UIDevice currentDevice].systemVersion doubleValue]>=9.0)
#define IOS10 ([[UIDevice currentDevice].systemVersion doubleValue]>=10.0)
#define IOS11 ([[UIDevice currentDevice].systemVersion doubleValue]>=11.0)

/** 判断是否为iPhone */
#define isiPhone (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

/** 判断是否是iPad */
#define isiPad (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

/** 判断是否为iPod */
#define isiPod ([[[UIDevice currentDevice] model] isEqualToString:@"iPod touch"])

//判断iPhoneX
#define IS_IPHONE_X ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) && !isiPad : NO)
//判断iPHoneXr
#define IS_IPHONE_Xr ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(828, 1792), [[UIScreen mainScreen] currentMode].size) && !isiPad : NO)
//判断iPhoneXs
#define IS_IPHONE_Xs ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) && !isiPad : NO)
//判断iPhoneXs Max
#define IS_IPHONE_Xs_Max ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2688), [[UIScreen mainScreen] currentMode].size) && !isiPad : NO)

#define IS_IPHONEXX_Series \
({BOOL isPhoneX = NO;\
if (@available(iOS 11.0, *)) {\
isPhoneX = [[UIApplication sharedApplication] delegate].window.safeAreaInsets.bottom > 0.0;\
}\
(isPhoneX);})


//适配iPhoneX  电池栏高度不一致
#define KStatusHeight (IS_IPHONEXX_Series?(44.f):(20.f))

#define BottomSafeAreaHeight 34.0

#define kTabBarHeight (IS_IPHONEXX_Series?(49.f+BottomSafeAreaHeight):(49.f))

#define kTabBarOffsetIphoneX (IS_IPHONEXX_Series?(0.0f+BottomSafeAreaHeight):(0.0f))

#define kNavigaionBarHeight (IS_IPHONEXX_Series?(88.f):(64.f))


#define SYSTEM_VERSION [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]


//颜色类
#define RGBA(r,g,b,a)     [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a]
#define RGB(r, g, b)      RGBA(r,g,b,1.0f)

#define HEXCOLOR(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define HEXACOLOR(rgbValue,a) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]

#define Color_white      [UIColor whiteColor] //白色
#define Color_black      [UIColor blackColor] //黑色
#define Color_clear      [UIColor clearColor] //透明
#define Color_red        HEXCOLOR(0xFF0000)   //主题红色
#define Color_title_222  HEXCOLOR(0x222222)   //主题标题色1
#define Color_title_333  HEXCOLOR(0x333333)   //主题标题色2
#define Color_text_999   HEXCOLOR(0x999999)   //主题文字色1
#define Color_text_666   HEXCOLOR(0x666666)   //主题文字色2
#define Color_backgound  HEXCOLOR(0xF5F5F5)   //主题页面底色
#define Color_line       HEXCOLOR(0xE5E5E5)   //分割线色
#define Color_Yellow     RGB(254, 245, 230)   //控件黄色
#define Color_text_Yellow     RGB(193, 151, 100)   //文字黄色


//字符串是否为空
#define EmptyStr(str) ([str isKindOfClass:[NSNull class]] || str == nil || [str length] < 1 ? YES : NO)
#define EmptyStrNotNil(string)  (EmptyStr(string) ? @"" : string)

//数组是否为空
#define EmptyArr(array) (array == nil || [array isKindOfClass:[NSNull class]] || array.count == 0)
//字典是否为空
#define EmptyDic(dic) (dic == nil || [dic isKindOfClass:[NSNull class]] || dic.allKeys == 0)
//是否是空对象
#define EmptyObj(_object) (_object == nil \
|| [_object isKindOfClass:[NSNull class]] \
|| ([_object respondsToSelector:@selector(length)] && [(NSData *)_object length] == 0) \
|| ([_object respondsToSelector:@selector(count)] && [(NSArray *)_object count] == 0))

// 日志输出
#ifdef DEBUG
#define NSLog(...) NSLog(__VA_ARGS__)
#else
#define NSLog(...)
#endif

//app下载地址
#define UpdataPath @""

//app通用下载链接
#define downLoadLink @""

// 通知
#define RegisterNotify(_name, _selector)                    \
[[NSNotificationCenter defaultCenter] addObserver:self selector:_selector name:_name object:nil]

#define RemoveNofify            \
[[NSNotificationCenter defaultCenter] removeObserver:self]

#define SendNotify(_name, _object)  \
[[NSNotificationCenter defaultCenter] postNotificationName:_name object:_object]

#define SendUserInfoNotify(_name, _object, _userInfo)  \
[[NSNotificationCenter defaultCenter] postNotificationName:_name object:_object userInfo:_userInfo]


//获取temp
#define TempPath NSTemporaryDirectory()
//获取沙盒 Document
#define DocumentPath [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject]
//获取沙盒 Cache
#define CachePath [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject]

/**
 *  the saving objects      存储对象
 *
 *  @param __VALUE__ V
 *  @param __KEY__   K
 *
 *  @return
 */
#define UserDefaultSetObjectForKey(__VALUE__,__KEY__) \
{\
[[NSUserDefaults standardUserDefaults] setObject:__VALUE__ forKey:__KEY__];\
[[NSUserDefaults standardUserDefaults] synchronize];\
}

/**
 *  get the saved objects       获得存储的对象
 */
#define UserDefaultObjectForKey(__KEY__)  [[NSUserDefaults standardUserDefaults] objectForKey:__KEY__]

/**
 *  delete objects      删除对象
 */
#define UserDefaultRemoveObjectForKey(__KEY__) \
{\
[[NSUserDefaults standardUserDefaults] removeObjectForKey:__KEY__];\
[[NSUserDefaults standardUserDefaults] synchronize];\
}

#define kImgHolder  @"img_holder"

// 登录状态通知
#define UserLoginStatusChange   @"userLoginStatusChangedNotification"
// 刷新收货地址
#define RefreshAddressNotify   @"refreshAddressNotification"



#define kTokenServiceName  @"tokenServiceName"
#define kTokenAccountName  @"tokenAccountName"

//微信appID appSecret
#define KWXAppID @"wxd2b997f12a0fa874"
#define KWXAppSecret @"9a4de5a62170bfa0790ad90904a33421"
#define KWXUniversalLink @"https://jm.api.dxanm.com/apple-app-site-association/"




#endif /* CBEMacros_h */
