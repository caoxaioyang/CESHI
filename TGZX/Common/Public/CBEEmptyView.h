//
//  CBEEmptyView.h
//  CBECommerce
//
//  Created by Eleven_Liu on 2020/5/18.
//  Copyright © 2020 Eleven_Liu. All rights reserved.
//

#import "LYEmptyView.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBEEmptyView : LYEmptyView

///图片和文字
+ (instancetype)CBENoDataEmptyView;

//自定义图片和文字
+ (instancetype)CBENoDataEmptyViewWithImgName:(NSString *)imgName text:(NSString *)text;

///网络错误
+ (instancetype)CBENoNetworkEmptyWithTarget:(id)target action:(SEL)action;


@end

NS_ASSUME_NONNULL_END
