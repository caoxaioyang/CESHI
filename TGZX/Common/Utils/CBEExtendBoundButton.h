//
//  CBEExtendBoundButton.h
//  CBECommerce
//
//  Created by Eleven_Liu on 2020/5/26.
//  Copyright © 2020 Eleven_Liu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBEExtendBoundButton : UIButton
///点击区域，默认为（0，0，0，0）; 负的为扩大
@property (nonatomic,assign) UIEdgeInsets hitTestEdgeInsets;

@end

NS_ASSUME_NONNULL_END
