//
//  CBEExtendBoundButton.m
//  CBECommerce
//
//  Created by Eleven_Liu on 2020/5/26.
//  Copyright © 2020 Eleven_Liu. All rights reserved.
//

#import "CBEExtendBoundButton.h"

@implementation CBEExtendBoundButton

//扩大点击区域
- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    if(UIEdgeInsetsEqualToEdgeInsets(self.hitTestEdgeInsets, UIEdgeInsetsZero) || !self.enabled || self.hidden)
    {
        return [super pointInside:point withEvent:event];
    }
    
    CGRect relativeFrame = self.bounds;
    CGRect hitFrame = UIEdgeInsetsInsetRect(relativeFrame, self.hitTestEdgeInsets);
    
    return CGRectContainsPoint(hitFrame, point);
}

@end
