//
//  UIImageView+Extension.h
//  CBECommerce
//
//  Created by Eleven_Liu on 2020/5/29.
//  Copyright © 2020 Eleven_Liu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^UIImageViewCompletionBlock)(UIImage *image);

@interface UIImageView (Extension)

- (void)setImageWithURLString:(NSString*)string;

- (void)setImageWithURLString:(NSString*)string completionBlock:(UIImageViewCompletionBlock)block;

- (void)setImageWithURLString:(NSString *)string placeholderImage:(UIImage *)placeholderImage;

- (void)setImageWithURLString:(NSString *)string placeholderImage:(UIImage *)placeholderImage completionBlock:(UIImageViewCompletionBlock)block;

@end

NS_ASSUME_NONNULL_END
