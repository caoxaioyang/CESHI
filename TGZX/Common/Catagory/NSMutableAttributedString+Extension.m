

#import "NSMutableAttributedString+Extension.h"
#import "NSMutableParagraphStyle+Extension.h"
#include <objc/runtime.h>

@implementation NSMutableAttributedString (Extension)

#pragma mark - 返回AttributedString属性
+ (NSMutableAttributedString *)attributeWithStr:(NSString *)str {
    return [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",str]];
}

#pragma mark - 需要修改的字符颜色数组及量程，由字典组成  key = 颜色   value = 量程或需要修改的字符串
- (void)colorsOfRanges:(NSArray<NSDictionary *> *)colorsOfRanges {
    if(colorsOfRanges == nil) return;
    for (NSDictionary *dic in colorsOfRanges) {
        UIColor *color = (UIColor *)[dic.allKeys firstObject];
        if ([[dic.allValues firstObject] isKindOfClass:[NSString class]]) {
            NSString *rangeStr = (NSString *)[dic.allValues firstObject];
            [self addAttribute:NSForegroundColorAttributeName value:color range:[self.string rangeOfString:rangeStr]];
        } else {
            NSArray *rangeAry = (NSArray *)[dic.allValues firstObject];
            [self addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange([[rangeAry firstObject] integerValue], [[rangeAry lastObject] integerValue])];
        }
    }
}

#pragma mark - 需要修改的字符字体数组及量程，由字典组成  key = 颜色   value = 量程或需要修改的字符串
- (void)fontsOfRanges:(NSArray<NSDictionary *> *)fontsOfRanges {
    if(fontsOfRanges == nil) return;
    for (NSDictionary *dic in fontsOfRanges) {
        UIFont *font = (UIFont *)[dic.allKeys firstObject];
        if ([[dic.allValues firstObject] isKindOfClass:[NSString class]]) {
            NSString *rangeStr = (NSString *)[dic.allValues firstObject];
            [self addAttribute:NSFontAttributeName value:font range:[self.string rangeOfString:rangeStr]];
        } else {
            NSArray *rangeAry = (NSArray *)[dic.allValues firstObject];
            [self addAttribute:NSFontAttributeName value:font range:NSMakeRange([[rangeAry firstObject] integerValue], [[rangeAry lastObject] integerValue])];
        }
    }
}

#pragma mark - 设置行间距
- (void)setLineSpacing:(CGFloat)lineSpacing string:(NSString *)string {
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle paragraphStyle];
    [paragraphStyle setLineSpacing:lineSpacing];
    [self addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:[[NSString stringWithFormat:@"%@",self.string] rangeOfString:[NSString stringWithFormat:@"%@",string]]];
}

#pragma mark - 设置字间距
- (void)setWordsSpacing:(CGFloat)wordsSpacing string:(NSString *)string {
    [self addAttribute:NSKernAttributeName value:[NSNumber numberWithFloat:wordsSpacing] range:[self.string rangeOfString:string]];
}

#pragma mark - 设置文字方向
- (void)setAlignment:(NSTextAlignment)textAlignment {
    NSMutableParagraphStyle *paragraphStyle = ([self attribute:NSParagraphStyleAttributeName atIndex:0 effectiveRange:nil]);
    if(paragraphStyle == nil) {
        paragraphStyle = [NSMutableParagraphStyle paragraphStyle];
    }
    paragraphStyle.alignment = textAlignment;
    [self addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, self.string.length)];
}

#pragma mark - 添加下划线
- (void)addUnderlineWithString:(NSString *)string {
    [self addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:[self.string rangeOfString:string]];
}

#pragma mark - 添加中划线
- (void)addHorizontalLineWithString:(NSString *)string {
    [self addAttribute:NSStrikethroughStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:[self.string rangeOfString:string]];
}

@end
