//
//  UIImage+Extension.h
//  CBECommerce
//
//  Created by Eleven_Liu on 2020/5/18.
//  Copyright © 2020 Eleven_Liu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (Extension)

// color image
+ (nullable UIImage *)imageWithColor:(nullable UIColor *)color;
+ (nullable UIImage *)imageWithColor:(nullable UIColor *)color size:(CGSize)size;

//压缩成指定大小
-(NSData *)compressWithMaxLength:(NSUInteger)maxLength;

@end

NS_ASSUME_NONNULL_END
