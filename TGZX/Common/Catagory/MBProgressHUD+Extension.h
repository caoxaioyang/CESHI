//
//  MBProgressHUD+Extension.h
//  CBECommerce
//
//  Created by jifan on 2020/5/19.
//  Copyright © 2020 Eleven_Liu. All rights reserved.
//

#import "MBProgressHUD.h"

NS_ASSUME_NONNULL_BEGIN

@interface MBProgressHUD (Extension)
+ (void)showLoading;
+ (void)showLoadingWithText:(NSString *)text;
+ (void)showLoadingWithText:(NSString *)text detailText:(NSString *)detailText;

+ (void)showCustomLoadingAddedTo:(UIView *)view animated:(BOOL)animated;
+ (void)showCustomLoading1;
+ (void)hideLoading;
+ (void)showMessageWithText:(NSString *)text;
+ (void)showMessageWithText:(NSString *)text detailText:(NSString *)detailText;

+ (void)showCustomViewWithImgName:(NSString *)imgName Text:(NSString *)text;
+ (void)showCustomViewWithImgName:(NSString *)imgName Text:(NSString *)text detailText:(NSString *)detailText;

+ (void)showMessageWithBottomText:(NSString *)text;
+ (void)showMessageWithBottomImgName:(NSString *)imgName;
+ (void)showMessageWithTopText:(NSString *)text;
@end

NS_ASSUME_NONNULL_END
