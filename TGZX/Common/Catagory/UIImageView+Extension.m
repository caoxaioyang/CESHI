//
//  UIImageView+Extension.m
//  CBECommerce
//
//  Created by Eleven_Liu on 2020/5/29.
//  Copyright © 2020 Eleven_Liu. All rights reserved.
//

#import "UIImageView+Extension.h"

@implementation UIImageView (Extension)

- (void)setImageWithURLString:(NSString *)string {
    [self sd_setImageWithURL:[NSURL URLWithString:string]];
}

- (void)setImageWithURLString:(NSString *)string completionBlock:(UIImageViewCompletionBlock)block {
    
    [self sd_setImageWithURL:[NSURL URLWithString:string]
            placeholderImage:[UIImage imageNamed:@"product_holder"]
                   completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                       block(image);
                   }];
    [self sd_setImageWithURL:[NSURL URLWithString:string] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        block(image);
    }];
}

- (void)setImageWithURLString:(NSString *)string placeholderImage:(UIImage *)placeholderImage {
    
    [self sd_setImageWithURL:[NSURL URLWithString:string] placeholderImage:placeholderImage];
}

- (void)setImageWithURLString:(NSString *)string placeholderImage:(UIImage *)placeholderImage completionBlock:(UIImageViewCompletionBlock)block {
    [self sd_setImageWithURL:[NSURL URLWithString:string]
            placeholderImage:[UIImage imageNamed:@"product_holder"]
                   completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                       block(image);
                   }];
}

@end
