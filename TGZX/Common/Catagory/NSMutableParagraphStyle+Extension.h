
#import <UIKit/UIKit.h>

@interface NSMutableParagraphStyle (Extension)

/** 返回paragraphStyle属性 */
+ (NSMutableParagraphStyle *)paragraphStyle;

@end
