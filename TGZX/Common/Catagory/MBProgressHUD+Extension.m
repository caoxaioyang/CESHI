//
//  MBProgressHUD+Extension.m
//  CBECommerce
//
//  Created by jifan on 2020/5/19.
//  Copyright © 2020 Eleven_Liu. All rights reserved.
//

#import "MBProgressHUD+Extension.h"

@implementation MBProgressHUD (Extension)

+ (void)showLoading {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].delegate.window animated:YES];
    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;//透明
    hud.bezelView.color = [UIColor colorWithWhite:0 alpha:0.8];
    hud.contentColor = Color_white;
}
+ (void)showLoadingWithText:(NSString *)text {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].delegate.window animated:YES];
    hud.label.text = text;
    hud.label.textColor = [UIColor whiteColor];
    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;//透明
    hud.bezelView.color = [UIColor colorWithWhite:0 alpha:0.8];
    hud.contentColor = Color_white;
}
+ (void)showLoadingWithText:(NSString *)text detailText:(NSString *)detailText {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].delegate.window animated:YES];
    hud.label.text = text;
    hud.label.textColor = [UIColor whiteColor];
    hud.detailsLabel.text = detailText;
    hud.detailsLabel.textColor = [UIColor whiteColor];
    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;//透明
    hud.bezelView.color = [UIColor colorWithWhite:0 alpha:0.8];
    hud.contentColor = Color_white;
}


+ (void)showCustomLoadingAddedTo:(UIView *)view animated:(BOOL)animated {
    if (view == nil) view = [UIApplication sharedApplication].keyWindow;
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:animated];
    hud.mode = MBProgressHUDModeCustomView;
    UIImageView *loadingImg = [UIImageView new];
    NSMutableArray *imageArray = [NSMutableArray array];
    for (int i = 1; i <= 75; i++) {
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"loading%d", i]];
        [imageArray addObject:image ? image:@""];
    }
    loadingImg.animationDuration = 1.5;
    loadingImg.animationRepeatCount = 0;
    loadingImg.animationImages = imageArray;
    [loadingImg startAnimating];
    hud.customView = loadingImg;
//    hud.label.text = @"玩命加载中...";
    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;//透明
    hud.bezelView.color = [UIColor colorWithWhite:0 alpha:0];
}

+ (void)showCustomLoading1 {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    hud.mode = MBProgressHUDModeCustomView;
    UIImageView *loadingImg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"custom_loading"]];
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform"];
    animation.fromValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
    animation.toValue = [NSValue valueWithCATransform3D: CATransform3DMakeRotation(M_PI_2, 0.0, 0.0, 1.0)];
    animation.duration = 0.25;
    animation.cumulative = YES;
    animation.repeatCount = MAXFLOAT;
    [loadingImg.layer addAnimation:animation forKey:@"rotationAnimation"];
    hud.customView = loadingImg;
//    hud.label.text = @"玩命加载中...";
//    hud.label.textColor = [UIColor whiteColor];
    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;//透明
    hud.bezelView.color = [UIColor colorWithWhite:0 alpha:0.8];
}

+ (void)hideLoading{
    [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].delegate.window animated:YES];
}

+ (void)showMessageWithText:(NSString *)text {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.label.text = text;
    hud.label.textColor = [UIColor whiteColor];
    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;//透明
    hud.bezelView.color = [UIColor colorWithWhite:0 alpha:0.8];
    [hud hideAnimated:YES afterDelay:1.5];
}

+ (void)showMessageWithText:(NSString *)text detailText:(NSString *)detailText {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.label.text = text;
    hud.label.textColor = [UIColor whiteColor];
    hud.detailsLabel.text = detailText;
    hud.detailsLabel.textColor = [UIColor whiteColor];
    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;//透明
    hud.bezelView.color = [UIColor colorWithWhite:0 alpha:0.8];
    [hud hideAnimated:YES afterDelay:1.5];
}

+ (void)showCustomViewWithImgName:(NSString *)imgName Text:(NSString *)text {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    hud.mode = MBProgressHUDModeCustomView;
    hud.customView = [[UIImageView alloc]initWithImage:[[UIImage imageNamed:imgName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    hud.label.text = text;
    hud.label.textColor = [UIColor whiteColor];
    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;//透明
    hud.bezelView.color = [UIColor colorWithWhite:0 alpha:0.8];
    [hud hideAnimated:YES afterDelay:1.5];
}

+ (void)showCustomViewWithImgName:(NSString *)imgName Text:(NSString *)text detailText:(NSString *)detailText{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    hud.mode = MBProgressHUDModeCustomView;
    hud.customView = [[UIImageView alloc]initWithImage:[[UIImage imageNamed:imgName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    hud.label.text = text;
    hud.label.textColor = [UIColor whiteColor];
    hud.detailsLabel.text = detailText;
    hud.detailsLabel.textColor = [UIColor whiteColor];
    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;//透明
    hud.bezelView.color = [UIColor colorWithWhite:0 alpha:0.8];
    [hud hideAnimated:YES afterDelay:1.5];
}

+ (void)showMessageWithBottomText:(NSString *)text{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.label.text = text;
    hud.label.textColor = [UIColor whiteColor];
    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;//透明
    hud.bezelView.color = [UIColor colorWithWhite:0 alpha:0.8];
    hud.offset = CGPointMake(0.f, MBProgressMaxOffset);
    [hud hideAnimated:YES afterDelay:1.5];
}

+ (void)showMessageWithBottomImgName:(NSString *)imgName {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    hud.mode = MBProgressHUDModeCustomView;
    hud.customView = [[UIImageView alloc]initWithImage:[[UIImage imageNamed:imgName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;//透明
    hud.bezelView.color = [UIColor clearColor];
    hud.offset = CGPointMake(0.f, SCREEN_HEIGHT/2-150);
    [hud hideAnimated:YES afterDelay:1.5];
}

+ (void)showMessageWithTopText:(NSString *)text{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.label.text = text;
    hud.label.textColor = [UIColor whiteColor];
    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;//透明
    hud.bezelView.color = [UIColor colorWithWhite:0 alpha:0.75];
    hud.offset = CGPointMake(0.f, -SCREEN_WIDTH/2-64);
    hud.margin = 10;
    [hud hideAnimated:YES afterDelay:2.5];
}

@end
