

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITextField (Extension)

/**
 *  占位文字颜色
 */
@property(strong,nonatomic) UIColor *placeholderColor;

/**
 *  清除按钮图片
 */
@property(strong,nonatomic) UIImage *clearButtonImage;

/**
 *  文本最大支持多少个字符，设置后会自动根据该属性截取文本字符长度
 */
@property(assign,nonatomic) NSInteger maximumLimit;

/**
 *  是否禁止复制粘贴 默认允许
 */
@property(assign,nonatomic) BOOL allowCopyPaste;

/**
 *  文本发生改变时回调
 */
- (void)textDidChange:(void(^)(NSString *textStr))handle;

/**
 *  处理系统输入法导致的乱码
 */
- (void)fixMessyDisplay;

@end

NS_ASSUME_NONNULL_END
