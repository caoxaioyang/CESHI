//
//  CBEInputVerifyTool.h
//  CBECommerce
//
//  Created by jifan on 2020/5/19.
//  Copyright © 2020 Eleven_Liu. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBEInputVerifyTool : NSObject

/**
 * 精确判断身份证号码
 */
+ (BOOL)checkIDCardNumber:(NSString *)identityCard;
/**
 * 验证手机号码
 */
+ (NSString *)checkMobileNumber:(NSString *)mobile;
/**
 * 验证用户密码  8-20位数字和字母组合
 */
+ (BOOL)checkLoginPassword:(NSString*)password;
/**
 * 验证支付密码  6位数字
 */
+ (BOOL)checkPayPassword:(NSString*)password;
/**
 * 验证短信验证码
 */
+ (BOOL)checkSMSCode:(NSString *)SMSCode;
/**
 * 验证图片验证码
 */
+ (BOOL)checkPictureCode:(NSString *)PictureCode;
/**
 * 验证是否为银行卡号
 */
+ (BOOL)checkCardNo:(NSString*)cardNo;
/**
 * 验证联系人现居地址
 */
+ (BOOL)checkCurrentAddress:(NSString*)currentAddress;
/**
 * 验证详细地址地址
 */
+ (BOOL)checkDetailAddress:(NSString*)detailAddress;
/**
 * 验证工作收入
 */
+ (BOOL)checkIncome:(NSString*)income;


@end

NS_ASSUME_NONNULL_END
