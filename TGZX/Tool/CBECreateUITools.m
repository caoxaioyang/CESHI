//
//  CBECreateUITools.m
//  CBECommerce
//
//  Created by Eleven_Liu on 2020/5/18.
//  Copyright © 2020 Eleven_Liu. All rights reserved.
//

#import "CBECreateUITools.h"

@implementation CBECreateUITools

#pragma mark - 生成一个Label
+ (UILabel *)createOneLabel:(CGRect)frame title:(NSString *)title superView:(UIView *)superView font:(UIFont *)font textColor:(UIColor *)color
{
    UILabel *oneLabel = [[UILabel alloc]initWithFrame:frame];
    oneLabel.text = title;
    oneLabel.font = font;
    oneLabel.textColor = color;
    [superView addSubview:oneLabel];
    return oneLabel;
}
//masonry
+ (UILabel *)createOneLabel:(NSString *)title superView:(UIView *)superView font:(UIFont *)font textColor:(UIColor *)color {
    UILabel *oneLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    oneLabel.text = title;
    oneLabel.font = font;
    oneLabel.textColor = color;
    [superView addSubview:oneLabel];
    return oneLabel;
}

+ (UILabel *)createOneLabel:(CGRect)frame title:(NSString *)title superView:(UIView *)superView font:(UIFont *)font textColor:(UIColor *)color textAlignment:(NSTextAlignment)textAlignment
{
    UILabel *oneLabel = [[UILabel alloc]initWithFrame:frame];
    oneLabel.text = title;
    oneLabel.font = font;
    oneLabel.textColor = color;
    oneLabel.textAlignment = textAlignment;
    [superView addSubview:oneLabel];
    return oneLabel;
}
//masonry
+ (UILabel *)createOneLabel:(NSString *)title superView:(UIView *)superView font:(UIFont *)font textColor:(UIColor *)color textAlignment:(NSTextAlignment)textAlignment {
    UILabel *oneLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    oneLabel.text = title;
    oneLabel.font = font;
    oneLabel.textColor = color;
    oneLabel.textAlignment = textAlignment;
    [superView addSubview:oneLabel];
    return oneLabel;
}

#pragma mark - 生成一个ImageView
+ (UIImageView *)createImageView:(CGRect)frame image:(UIImage * _Nullable)image superView:(UIView *)superView
{
    UIImageView *ImgView = [[UIImageView alloc]initWithFrame:frame];
    ImgView.image = image;
    [superView addSubview:ImgView];
    return ImgView;
}
//masonry
+ (UIImageView *)createImageView:(UIImage * _Nullable)image superView:(UIView *)superView {
    UIImageView *ImgView = [[UIImageView alloc]initWithFrame:CGRectZero];
    ImgView.image = image;
    [superView addSubview:ImgView];
    return ImgView;
}


#pragma mark - 生成一个UIButton
+ (UIButton *)createUIButton:(CGRect)frame image:(UIImage * _Nullable)image title:(NSString * _Nullable)title backgroundColor:(UIColor * _Nullable)color superView:(UIView *)superView
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = frame;
    [btn setImage:image forState:UIControlStateNormal];
    [btn setTitle:title forState:UIControlStateNormal];
    btn.backgroundColor = color;
    [superView addSubview:btn];
    return btn;
}
//masonry
+ (UIButton *)createUIButton:(UIImage * _Nullable)image title:(NSString * _Nullable)title backgroundColor:(UIColor * _Nullable)color superView:(UIView *)superView {
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectZero;
    [btn setImage:image forState:UIControlStateNormal];
    [btn setTitle:title forState:UIControlStateNormal];
    btn.backgroundColor = color;
    [superView addSubview:btn];
    return btn;
}

+ (CBEExtendBoundButton *)createExtendBoundUIButton:(UIImage * _Nullable)image title:(NSString * _Nullable)title backgroundColor:(UIColor * _Nullable)color edgeInsets:(UIEdgeInsets)edg superView:(UIView *)superView
{
    CBEExtendBoundButton *btn = [CBEExtendBoundButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectZero;
    [btn setImage:image forState:UIControlStateNormal];
    [btn setTitle:title forState:UIControlStateNormal];
    btn.backgroundColor = color;
    btn.hitTestEdgeInsets = edg;
    [superView addSubview:btn];
    return btn;
}

#pragma mark - 生成一个UIView
+ (UIView *)createUIView:(CGRect)frame backgroundColor:(UIColor *)color superView:(UIView *)superView
{
    UIView *view = [[UIView alloc]initWithFrame:frame];
    view.backgroundColor = color;
    [superView addSubview:view];
    return view;
}
//masonry
+ (UIView *)createUIViewBackgroundColor:(UIColor *)color superView:(UIView *)superView {
    UIView *view = [[UIView alloc]initWithFrame:CGRectZero];
    view.backgroundColor = color;
    [superView addSubview:view];
    return view;
}


@end
