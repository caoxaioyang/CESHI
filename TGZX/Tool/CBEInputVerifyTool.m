//
//  CBEInputVerifyTool.m
//  CBECommerce
//
//  Created by jifan on 2020/5/19.
//  Copyright © 2020 Eleven_Liu. All rights reserved.
//

#import "CBEInputVerifyTool.h"

@implementation CBEInputVerifyTool

/**
 * 精确判断身份证号码
 */
+ (BOOL)checkIDCardNumber:(NSString *)value {
    value = [value stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSInteger length = 0;
    if (!value) {
        return NO;
    }else {
        length = value.length;
        
        if (length !=18) {
            return NO;
        }
    }

    return YES;
}

/**
 * 验证手机号码
 */
+ (NSString *)checkMobileNumber:(NSString *)mobile {
    if (mobile.length < 11)
    {
        return @"请输入11位手机号";
    }else{
        NSString *MOBILE = @"^1\\d{10}$";
        NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
        if ([regextestmobile evaluateWithObject:mobile] == YES) {
            return nil;
        } else {
            return @"请输入正确的手机号码";
        }
    }
    return nil;
}

/**
 * 验证用户密码8-20位数字和字母组合
 */
+ (BOOL)checkLoginPassword:(NSString*)password {
    NSString *pattern =@"^(?![0-9]+$)(?![a-zA-Z]+$)[a-zA-Z0-9]{8,20}$";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",pattern];
    BOOL isMatch = [pred evaluateWithObject:password];
    return !isMatch;
}

/**
 * 验证支付密码  6位数字
 */
+ (BOOL)checkPayPassword:(NSString*)password {
    NSString *pattern = @"^[0-9]{6}$";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",pattern];
    BOOL isMatch = [pred evaluateWithObject:password];
    return !isMatch;
}

/**
 * 验证短信验证码 只能4位数学
 */
+ (BOOL)checkSMSCode:(NSString *)SMSCode {
    NSString *pattern = @"^[0-9]{4,6}$";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",pattern];
    BOOL isMatch = [pred evaluateWithObject:SMSCode];
    return !isMatch;
}

/**
 * 验证图片验证码
 */
+ (BOOL)checkPictureCode:(NSString *)pictureCode {
    pictureCode = [self trimTest:pictureCode];
    NSString *pattern = @"^[a-zA-Z0-9]{4}$";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",pattern];
    BOOL isMatch = [pred evaluateWithObject:pictureCode];
    return !isMatch;
}

/**
 * 验证是否为银行卡号
 */
+ (BOOL)checkCardNo:(NSString*)cardNo {
    if (cardNo.length < 16 || cardNo.length > 19) {
        return NO;
    }
//    int sum = 0;
//    NSInteger len = [cardNo length];
//    int i = 0;
//    while (i < len) {
//        NSString *tmpString = [cardNo substringWithRange:NSMakeRange(len - 1 - i, 1)];
//        int tmpVal = [tmpString intValue];
//        if (i % 2 != 0) {
//            tmpVal *= 2;
//            if(tmpVal>=10) {
//                tmpVal -= 9;
//            }
//        }
//        sum += tmpVal;
//        i++;
//    }
//    if((sum % 10) == 0)
        return YES;
//    else
//        return NO;
}
/**
 * 验证联系人现居地址
 */
+ (BOOL)checkCurrentAddress:(NSString*)currentAddress {
    if (currentAddress.length > 100) {
        return NO;
    }
    return YES;
}
/**
 * 验证详细地址地址
 */
+ (BOOL)checkDetailAddress:(NSString*)detailAddress {
    if (detailAddress.length > 200) {
        return NO;
    }
    return YES;
}
/**
 * 验证工作收入
 */
+ (BOOL)checkIncome:(NSString*)income {
    if (income.length > 20 ||income.length < 1) {
        return NO;
    }
    if ([income containsString:@"."]) {
        return NO;
    }
    return YES;
}


+ (NSString *)trimTest:(NSString *)text {
    return [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

@end
