//
//  CBECreateUITools.h
//  CBECommerce
//
//  Created by Eleven_Liu on 2020/5/18.
//  Copyright © 2020 Eleven_Liu. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CBEExtendBoundButton;


NS_ASSUME_NONNULL_BEGIN

@interface CBECreateUITools : NSObject

//------  label
+ (UILabel *)createOneLabel:(CGRect)frame title:(NSString *)title superView:(UIView *)superView font:(UIFont *)font textColor:(UIColor *)color;
//masonry
+ (UILabel *)createOneLabel:(NSString *)title superView:(UIView *)superView font:(UIFont *)font textColor:(UIColor *)color;

+ (UILabel *)createOneLabel:(CGRect)frame title:(NSString *)title superView:(UIView *)superView font:(UIFont *)font textColor:(UIColor *)color textAlignment:(NSTextAlignment)textAlignment;
//masonry
+ (UILabel *)createOneLabel:(NSString *)title superView:(UIView *)superView font:(UIFont *)font textColor:(UIColor *)color textAlignment:(NSTextAlignment)textAlignment;

//------  image
+ (UIImageView *)createImageView:(CGRect)frame image:(UIImage * _Nullable)image superView:(UIView *)superView;
//masonry
+ (UIImageView *)createImageView:(UIImage * _Nullable)image superView:(UIView *)superView;

///------  button
+ (UIButton *)createUIButton:(CGRect)frame image:(UIImage * _Nullable)image title:(NSString * _Nullable)title backgroundColor:(UIColor * _Nullable)color superView:(UIView *)superView;
//masonry
+ (UIButton *)createUIButton:(UIImage * _Nullable)image title:(NSString * _Nullable)title backgroundColor:(UIColor * _Nullable)color superView:(UIView *)superView;
//extend Btn
+ (CBEExtendBoundButton *)createExtendBoundUIButton:(UIImage * _Nullable)image title:(NSString * _Nullable)title backgroundColor:(UIColor * _Nullable)color edgeInsets:(UIEdgeInsets)edg superView:(UIView *)superView;


//------  UIView
+ (UIView *)createUIView:(CGRect)frame backgroundColor:(UIColor *)color superView:(UIView *)superView;
//masonry
+ (UIView *)createUIViewBackgroundColor:(UIColor *)color superView:(UIView *)superView;



@end

NS_ASSUME_NONNULL_END
