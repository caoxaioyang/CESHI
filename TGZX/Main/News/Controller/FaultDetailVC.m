//
//  FaultDetailVC.m
//  TGZX
//
//  Created by 曹晓阳 on 2021/10/26.
//

#import "FaultDetailVC.h"

@interface FaultDetailVC ()<UIScrollViewDelegate>
@property (nonatomic, strong) UIScrollView *scroll;
@property (nonatomic, strong) UIView *scrollContent;
@property (nonatomic, strong) UIView *headerV;
@property (nonatomic, strong) UIView *view1;
@property (nonatomic, strong) UIView *view2;

@end

@implementation FaultDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationTitleLabel.text = @"故障详情";
    [self setScroll];
    [self setView1];
    [self setView2];
    [self setView3];

}
- (void)setView3 {
    self.view2 = [CBECreateUITools createUIViewBackgroundColor:Color_white superView:self.scrollContent];
    
    UILabel *lab1 = [CBECreateUITools createOneLabel:@"故障描述" superView:self.view2 font:FONT_SIZE(14) textColor:Color_text_666];
    UILabel *lab2 = [CBECreateUITools createOneLabel:@"点火时断电 - 数据不稳定、间断或不正确。ECM 电源短暂降到 +6.2-VDC 以下，或 ECM 不可以正常断电（钥匙开关关闭后保持蓄电池电压 30 秒 ）" superView:self.view2 font:FONT_SIZE(16) textColor:Color_black];
    lab2.numberOfLines = 0;
    [self.view2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view1.mas_bottom).offset(20);
        make.left.right.mas_equalTo(0);
    }];
    [lab1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.top.mas_equalTo(20);
        make.height.mas_equalTo(20);
    }];
    
    [lab2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.top.mas_equalTo(lab1.mas_bottom).offset(10);
        make.right.mas_equalTo(-20);
        make.bottom.mas_equalTo(-20);
    }];
    
    
}
- (void)setView2 {
    self.view1 = [CBECreateUITools createUIViewBackgroundColor:Color_white superView:self.scrollContent];
    UILabel *left1 = [CBECreateUITools createOneLabel:@"报警信号" superView:self.view1 font:FONT_SIZE(14) textColor:Color_text_666];
    UILabel *left2 = [CBECreateUITools createOneLabel:@"SteeringValveCurrentFault" superView:self.view1 font:FONT_SIZE(16) textColor:Color_black];
    
    UILabel *left3 = [CBECreateUITools createOneLabel:@"阈值" superView:self.view1 font:FONT_SIZE(14) textColor:Color_text_666];
    UILabel *left4 = [CBECreateUITools createOneLabel:@"[40-50]" superView:self.view1 font:FONT_SIZE(16) textColor:Color_black];
    
    
    UILabel *right1 = [CBECreateUITools createOneLabel:@"当前值" superView:self.view1 font:FONT_SIZE(14) textColor:Color_text_666];
    UILabel *right2 = [CBECreateUITools createOneLabel:@"45" superView:self.view1 font:FONT_SIZE(16) textColor:Color_black];
    
    
    
    [self.view1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.headerV.mas_bottom).offset(20);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(145);
    }];
    [left1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(20);
        make.left.mas_equalTo(20);
        make.height.mas_equalTo(20);
    }];
    [left2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(left1.mas_bottom).offset(3);
        make.left.mas_equalTo(20);
        make.right.mas_equalTo(-20);
        make.height.mas_equalTo(20);
    }];
    [left3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(left2.mas_bottom).offset(15);
        make.left.mas_equalTo(20);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(SCREEN_WIDTH/2);
    }];
    [left4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(left3.mas_bottom).offset(3);
        make.left.mas_equalTo(20);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(SCREEN_WIDTH/2);
    }];
    [right1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(left2.mas_bottom).offset(15);
        make.left.mas_equalTo(left3.mas_right).offset(0);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(SCREEN_WIDTH/2);
    }];
    
    [right2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(right1.mas_bottom).offset(3);
        make.left.mas_equalTo(left4.mas_right).offset(0);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(SCREEN_WIDTH/2);
    }];
}
- (void)setView1 {
    self.headerV = [CBECreateUITools createUIViewBackgroundColor:Color_white superView:self.scrollContent];
    
    UILabel *left1 = [CBECreateUITools createOneLabel:@"故障类型" superView:self.headerV font:FONT_SIZE(14) textColor:Color_text_666];
    UILabel *left2 = [CBECreateUITools createOneLabel:@"故障等级" superView:self.headerV font:FONT_SIZE(14) textColor:Color_text_666];
    UILabel *left3 = [CBECreateUITools createOneLabel:@"发生时间" superView:self.headerV font:FONT_SIZE(14) textColor:Color_text_666];
    UILabel *left4 = [CBECreateUITools createOneLabel:@"发动机故障" superView:self.headerV font:FONT_SIZE(16) textColor:Color_black];
    UILabel *left5 = [CBECreateUITools createOneLabel:@"严重故障" superView:self.headerV font:FONT_SIZE(16) textColor:Color_black];
    UILabel *left6 = [CBECreateUITools createOneLabel:@"2021-01-01 12:00:00" superView:self.headerV font:FONT_SIZE(16) textColor:Color_black];
    
    UILabel *right1 = [CBECreateUITools createOneLabel:@"报警来源" superView:self.headerV font:FONT_SIZE(14) textColor:Color_text_666];
    UILabel *right2 = [CBECreateUITools createOneLabel:@"故障状态" superView:self.headerV font:FONT_SIZE(14) textColor:Color_text_666];
    UILabel *right3 = [CBECreateUITools createOneLabel:@"解除时间" superView:self.headerV font:FONT_SIZE(14) textColor:Color_text_666];
    UILabel *right4 = [CBECreateUITools createOneLabel:@"报警信号" superView:self.headerV font:FONT_SIZE(16) textColor:Color_black];
    UILabel *right5 = [CBECreateUITools createOneLabel:@"报警中" superView:self.headerV font:FONT_SIZE(16) textColor:Color_black];
    UILabel *right6 = [CBECreateUITools createOneLabel:@"2021-01-01 12:00:00" superView:self.headerV font:FONT_SIZE(16) textColor:Color_black];
    
    
    UILabel *left7 = [CBECreateUITools createOneLabel:@"最后上报时间" superView:self.headerV font:FONT_SIZE(14) textColor:Color_text_666];
    UILabel *left8 = [CBECreateUITools createOneLabel:@"2021-01-01 12:00:00" superView:self.headerV font:FONT_SIZE(16) textColor:Color_black];

    
    [self.headerV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(20);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(260);
    }];
    
    [right1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(20);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(SCREEN_WIDTH/2);
    }];
    [right4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(right1.mas_bottom).offset(3);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(SCREEN_WIDTH/2);
    }];
    [right2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(right4.mas_bottom).offset(15);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(SCREEN_WIDTH/2);
    }];
    [right5 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(right2.mas_bottom).offset(3);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(SCREEN_WIDTH/2);
    }];
    [right3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(right5.mas_bottom).offset(15);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(SCREEN_WIDTH/2);
    }];
    [right6 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(right3.mas_bottom).offset(3);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(SCREEN_WIDTH/2);
    }];
    
    [left1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(20);
        make.left.mas_equalTo(20);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(right1.mas_left);
    }];
    [left4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(left1.mas_bottom).offset(3);
        make.left.mas_equalTo(20);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(right4.mas_left);
    }];
    [left2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(left4.mas_bottom).offset(15);
        make.left.mas_equalTo(20);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(right2.mas_left);
    }];
    [left5 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(left2.mas_bottom).offset(3);
        make.left.mas_equalTo(20);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(right5.mas_left);
    }];
    [left3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(left5.mas_bottom).offset(15);
        make.left.mas_equalTo(20);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(right3.mas_left);
    }];
    [left6 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(left3.mas_bottom).offset(3);
        make.left.mas_equalTo(20);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(right6.mas_left);
    }];
    
    [left7 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(left6.mas_bottom).offset(15);
        make.left.mas_equalTo(20);
        make.height.mas_equalTo(20);
    }];
    [left8 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(left7.mas_bottom).offset(3);
        make.left.mas_equalTo(20);
        make.height.mas_equalTo(20);
    }];
    
}
- (void)setScroll {
    self.scroll = [[UIScrollView alloc] init];
    self.scroll.showsVerticalScrollIndicator = NO;
    self.scroll.showsHorizontalScrollIndicator = NO;
    self.scroll.bounces = NO;
    self.scroll.delegate = self;
    self.scroll.contentSize = CGSizeMake(SCREEN_WIDTH, 800);
    self.scrollContent = [UIView new];
    self.scrollContent.frame = CGRectMake(0, 0, SCREEN_WIDTH, 800);
    self.scrollContent.backgroundColor = Color_backgound;
    [self.scroll addSubview:self.scrollContent];
    [self.view addSubview:self.scroll];
    [self.scroll mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.mas_equalTo(0);
        make.top.mas_equalTo(self.navBGImgV.mas_bottom).offset(0);
    }];
}
@end
