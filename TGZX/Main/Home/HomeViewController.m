//
//  HomeViewController.m
//  TGZX
//
//  Created by 曹晓阳 on 2021/10/11.
//

#import "HomeViewController.h"
#import "IntentionViewController.h"

@interface HomeViewController ()<UIScrollViewDelegate,SDCycleScrollViewDelegate>
@property (nonatomic, strong) UIScrollView *scroll;
@property (nonatomic, strong) UIView *scrollContent;
@property (nonatomic, strong) UIView *headerV;
@property (nonatomic, strong) UIView *classifiV;
@property (nonatomic, strong) UIControl *noticeV;


@end

@implementation HomeViewController
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navBGImgV.hidden = YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setView];
    
}
- (void)setView {
    [self setScroll];
    [self setHeaderV];
    [self setClassification];
    [self setNotice];
    [self setNews];
}
- (void)setNews {
    UILabel *newsTitle = [CBECreateUITools createOneLabel:@"新闻中心" superView:self.scrollContent font:BOLD_SIZE(16) textColor:Color_black];
    NSArray *imagesURLStrings = @[@"https://www.baidu.com",@"https://www.baidu.com",@"https://www.baidu.com",@"https://www.baidu.com",@"https://www.baidu.com"];
    SDCycleScrollView *cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectZero delegate:self placeholderImage:[UIImage imageNamed:@"nte200"]];
    cycleScrollView.tag = 1;
    cycleScrollView.backgroundColor = Color_clear;
    cycleScrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
    cycleScrollView.imageURLStringsGroup = imagesURLStrings;
    cycleScrollView.autoScrollTimeInterval = 5;
    cycleScrollView.pageControlDotSize = CGSizeMake(20, 20);
    cycleScrollView.currentPageDotColor = Color_white;
    cycleScrollView.pageDotColor = RGB(178, 216, 252);
    cycleScrollView.layer.masksToBounds = YES;
    cycleScrollView.layer.cornerRadius = 4;
    [self.scrollContent addSubview:cycleScrollView];
    [newsTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.right.mas_equalTo(-17);
        make.top.equalTo(self.noticeV.mas_bottom).offset(15);
        make.height.mas_equalTo(25);
    }];
    [cycleScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(newsTitle.mas_bottom).offset(10);
        make.left.mas_equalTo(17);
        make.right.mas_equalTo(-17);
        make.height.mas_equalTo(160*kScreenWidthRatio);
    }];
}
- (void)setNotice {
    self.noticeV = [[UIControl alloc] init];
    self.noticeV.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.00];
    self.noticeV.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.15].CGColor;
    self.noticeV.layer.shadowOffset = CGSizeMake(0,0);
    self.noticeV.layer.shadowRadius = 5;
    self.noticeV.layer.shadowOpacity = 1;
    self.noticeV.layer.cornerRadius = 10;
    [self.scrollContent addSubview:self.noticeV];
    [self.noticeV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(17);
        make.right.mas_equalTo(-17);
        make.top.equalTo(self.classifiV.mas_bottom).offset(10);
        make.height.mas_equalTo(69*kScreenWidthRatio);
    }];
    
}


- (void)setClassification {
    self.classifiV = [UIView new];
    self.classifiV.backgroundColor = Color_backgound;
    self.classifiV.layer.cornerRadius = 10;
    [self.scrollContent addSubview:self.classifiV];
    
    [self.classifiV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headerV.mas_bottom).offset(-12);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(230*kScreenWidthRatio);
    }];

    NSArray *imgArr = @[@"home_sc",@"home_gz",@"home_yx",@"home_fx",@"home_dsj"];
    NSArray *titleArr = @[@"零件手册",@"故障查询",@"意向单",@"统计分析",@"大数据"];

    UIControl *control1 = [self createControl:imgArr[0] title:titleArr[0]];
    UIControl *control2 = [self createControl:imgArr[1] title:titleArr[1]];
    UIControl *control3 = [self createControl:imgArr[2] title:titleArr[2]];
    UIControl *control4 = [self createControl:imgArr[3] title:titleArr[3]];
    UIControl *control5 = [self createControl:imgArr[4] title:titleArr[4]];

    control1.tag = 0;
    control2.tag = 1;
    control3.tag = 2;
    control4.tag = 3;
    control5.tag = 4;

    [self.classifiV addSubview:control1];
    [self.classifiV addSubview:control2];
    [self.classifiV addSubview:control3];
    [self.classifiV addSubview:control4];
    [self.classifiV addSubview:control5];
    
    [control5 addTarget:self action:@selector(controlAction:) forControlEvents:UIControlEventTouchUpInside];
    [control1 addTarget:self action:@selector(controlAction:) forControlEvents:UIControlEventTouchUpInside];
    [control2 addTarget:self action:@selector(controlAction:) forControlEvents:UIControlEventTouchUpInside];
    [control3 addTarget:self action:@selector(controlAction:) forControlEvents:UIControlEventTouchUpInside];
    [control4 addTarget:self action:@selector(controlAction:) forControlEvents:UIControlEventTouchUpInside];

    int k = (SCREEN_WIDTH-60*kScreenWidthRatio*3)/4;
    [control1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(10);
        make.left.mas_equalTo(k);
        make.height.width.mas_equalTo(60*kScreenWidthRatio);
    }];
    [control2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(10);
        make.left.mas_equalTo(2*k +60*kScreenWidthRatio);
        make.height.width.mas_equalTo(60*kScreenWidthRatio);

    }];
    [control3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(10);
        make.left.mas_equalTo(3*k +2*60*kScreenWidthRatio);
        make.height.width.mas_equalTo(60*kScreenWidthRatio);
    }];
    [control4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(120);
        make.left.mas_equalTo(k);
        make.height.width.mas_equalTo(60*kScreenWidthRatio);
    }];
    [control5 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(120);
        make.left.mas_equalTo(2*k +60*kScreenWidthRatio);
        make.height.width.mas_equalTo(60*kScreenWidthRatio);
    }];
  
    
}
- (void)controlAction:(UIControl *)control {
    if (control.tag == 2) {
        //意向单
        IntentionViewController *intentionVC = [IntentionViewController new];
        [self.navigationController pushViewController:intentionVC animated:NO];
    }
}

- (UIControl *)createControl:(NSString *)imgStr title:(NSString *)title {
    UIControl *control = [[UIControl alloc] init];
    
    UIImageView *imgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgStr]];
    [control addSubview:imgV];
    UILabel *lab = [[UILabel alloc] init];
    lab.text = title;
    lab.font = FONT_SIZE(14);
    [control addSubview:lab];
    
    [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(10);
        make.centerX.mas_equalTo(0);
        make.height.width.mas_equalTo(kScreenWidthRatio*60);
    }];
    
    [lab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(imgV.mas_bottom).offset(10);
        make.centerX.mas_equalTo(0);
        make.height.mas_equalTo(20);
    }];
    return control;
}
- (void)setScroll {
    self.scroll = [[UIScrollView alloc] init];
    self.scroll.showsVerticalScrollIndicator = NO;
    self.scroll.showsHorizontalScrollIndicator = NO;
    self.scroll.bounces = NO;
    self.scroll.delegate = self;
    self.scroll.contentSize = CGSizeMake(SCREEN_WIDTH, 1000);
    self.scrollContent = [UIView new];
    self.scrollContent.frame = CGRectMake(0, 0, SCREEN_WIDTH, 1000);
    self.scrollContent.backgroundColor = Color_backgound;
    [self.scroll addSubview:self.scrollContent];
    [self.view addSubview:self.scroll];
    [self.scroll mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.right.mas_equalTo(0);
    }];
}
- (void)setHeaderV {
    self.headerV = [UIView new];
    [self.scrollContent addSubview:self.headerV];
    
    [self.headerV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(0);
        make.height.mas_equalTo(330*kScreenWidthRatio);
    }];
    UIImageView *headerBg = [CBECreateUITools createImageView:[UIImage imageNamed:@"home_bg"] superView:self.headerV];
    [headerBg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.bottom.right.mas_equalTo(0);
    }];
    
    NSArray *imagesURLStrings = @[@"https://www.baidu.com",@"https://www.baidu.com",@"https://www.baidu.com",@"https://www.baidu.com",@"https://www.baidu.com"];
    SDCycleScrollView *cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, kNavigaionBarHeight, SCREEN_WIDTH, 330*kScreenWidthRatio-kNavigaionBarHeight-12) delegate:self placeholderImage:[UIImage imageNamed:@"nte200"]];
    cycleScrollView.backgroundColor = Color_clear;
    cycleScrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
    cycleScrollView.imageURLStringsGroup = imagesURLStrings;
    cycleScrollView.autoScrollTimeInterval = 3;
    cycleScrollView.pageControlDotSize = CGSizeMake(20, 20);
    cycleScrollView.currentPageDotColor = Color_white;
    cycleScrollView.pageDotColor = RGB(178, 216, 252);
    cycleScrollView.layer.masksToBounds = YES;
    cycleScrollView.layer.cornerRadius = 4;
    [self.headerV addSubview:cycleScrollView];
}
#pragma mark - SDCycleScrollView

/** 点击图片回调 */
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    
}

/** 图片滚动回调 */
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didScrollToIndex:(NSInteger)index {
    
}
@end
