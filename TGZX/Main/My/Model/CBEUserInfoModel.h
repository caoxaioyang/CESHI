//
//  CBEUserInfoModel.h
//  CBECommerce
//
//  Created by jifan on 2020/5/19.
//  Copyright © 2020 Eleven_Liu. All rights reserved.
//

#import "CBEBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBEUserInfoModel : CBEBaseModel
@property (nonatomic, strong) NSNumber *userId;
@property (nonatomic, copy  ) NSString *nickname;
@property (nonatomic, copy  ) NSString *head_img;
@property (nonatomic, copy  ) NSString *mobile;
@property (nonatomic, copy  ) NSString *token;
@end

NS_ASSUME_NONNULL_END
