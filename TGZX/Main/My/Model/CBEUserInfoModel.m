//
//  CBEUserInfoModel.m
//  CBECommerce
//
//  Created by jifan on 2020/5/19.
//  Copyright © 2020 Eleven_Liu. All rights reserved.
//

#import "CBEUserInfoModel.h"

@implementation CBEUserInfoModel

+(NSArray<NSString *> *)setupDataFieldsIsModelArray:(BOOL *)isModelArray{
    return @[@"data"];
}

+(NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{@"userId":@"id"};
}

MJCodingImplementation

@end
