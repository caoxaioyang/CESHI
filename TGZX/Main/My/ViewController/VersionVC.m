//
//  VersionVC.m
//  TGZX
//
//  Created by 曹晓阳 on 2021/10/22.
//

#import "VersionVC.h"

@interface VersionVC ()

@end

@implementation VersionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationTitleLabel.text = @"版本信息";
    // app版本
    NSString *app_Version = [NSString stringWithFormat:@"%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
    UIView *view = [CBECreateUITools createUIViewBackgroundColor:Color_white superView:self.view];
    UIView *title = [CBECreateUITools createOneLabel:CGRectZero title:@"当前版本号" superView:view font:FONT_SIZE(14) textColor:Color_text_666];
    UIView *version = [CBECreateUITools createOneLabel:CGRectZero title:app_Version superView:view font:FONT_SIZE(14) textColor:Color_black];
    
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navBGImgV.mas_bottom).offset(10);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(76);
    }];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.top.mas_equalTo(10);
        make.height.mas_equalTo(20);
    }];
    [version mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(23);
        make.height.mas_equalTo(20);
        make.top.equalTo(title.mas_bottom).offset(5);
    }];

}

@end
