//
//  ChangePwdViewController.m
//  TGZX
//
//  Created by 曹晓阳 on 2021/10/22.
//

#import "ChangePwdViewController.h"

@interface ChangePwdViewController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vConstraint;
@property (weak, nonatomic) IBOutlet UIButton *codeBtn;
@property (weak, nonatomic) IBOutlet UIButton *sureBtn;

@end

@implementation ChangePwdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.vConstraint.constant = kNavigaionBarHeight+20;
    self.codeBtn.titleLabel.font = FONT_SIZE(11);
    self.codeBtn.layer.cornerRadius = 3;
    self.sureBtn.layer.cornerRadius = 5;
}

@end
