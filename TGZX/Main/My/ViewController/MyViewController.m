//
//  MyViewController.m
//  TGZX
//
//  Created by 曹晓阳 on 2021/10/21.
//

#import "MyViewController.h"
#import "VersionVC.h"
#import "CBELoadHtmlVC.h"
#import "ChangePwdViewController.h"

@interface MyViewController ()
@property (weak, nonatomic) IBOutlet UIView *bgV;

@property (weak, nonatomic) IBOutlet UIButton *icon;
@property (weak, nonatomic) IBOutlet UILabel *phone;

@end

@implementation MyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
   
    [self setView];
}
- (void)setView {
    self.navBGImgV.hidden = YES;
    self.bgV.layer.shadowColor = [UIColor colorWithRed:21/255.0 green:21/255.0 blue:21/255.0 alpha:0.15].CGColor;
    self.bgV.layer.shadowOffset = CGSizeMake(0,5);
    self.bgV.layer.shadowRadius = 10;
    self.bgV.layer.shadowOpacity = 1;
    // Radius Code
    self.bgV.layer.cornerRadius = 10;
    self.bgV.layer.masksToBounds = YES;
}
//修改密码
- (IBAction)changePwd:(UIControl *)sender {
    ChangePwdViewController *changePwd = [[ChangePwdViewController alloc] initWithNibName:@"ChangePwdViewController" bundle:nil];
    [self.navigationController pushViewController:changePwd animated:nil];
}
//版本信息
- (IBAction)versionInfor:(UIControl *)sender {
    VersionVC *vc = [VersionVC new];
    [self.navigationController pushViewController:vc animated:NO];
}
//服务条款
- (IBAction)ruleAction:(UIControl *)sender {
    CBELoadHtmlVC *html = [[CBELoadHtmlVC alloc] init];
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"read.html" withExtension:nil];
    html.urlString = url.absoluteString;
    [self.navigationController pushViewController:html animated:YES];
}
//点击头像
- (IBAction)iconAction:(UIButton *)sender {
    
}
//退出登录
- (IBAction)outLoginAction:(UIButton *)sender {
    
}

@end
