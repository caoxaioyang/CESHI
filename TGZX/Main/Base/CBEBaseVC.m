//
//  CBEBaseVC.m
//  CBECommerce
//
//  Created by Eleven_Liu on 2020/5/18.
//  Copyright © 2020 Eleven_Liu. All rights reserved.
//

#import "CBEBaseVC.h"

@interface CBEBaseVC ()

@end

@implementation CBEBaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //ViewController的背景颜色，如果整个App页面背景颜色比较统一，建议在这里设置
    self.view.backgroundColor = Color_backgound;
    
    self.navigationItem.hidesBackButton = YES;
    if (@available(iOS 11.0, *)) {
        //scrollerView在导航栏透明时不下压
        [[UIScrollView appearance] setContentInsetAdjustmentBehavior:UIScrollViewContentInsetAdjustmentNever];
    }else{
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    [self setup];
    [self masonry];
    
}


#pragma mark - ----------------- setupUI

- (void)setup {
    
    [self.view addSubview:self.navBGImgV];
    
    [self.navBGImgV addSubview:self.separatorView];
    
    [self.navBGImgV addSubview:self.backButton];
    
    [self.navBGImgV addSubview:self.rightButton];
    
    [self.navBGImgV addSubview:self.navigationTitleLabel];
    
   
}

- (void)masonry {
    
    [self.navBGImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.mas_equalTo(kNavigaionBarHeight);
    }];
    
    [self.separatorView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.navBGImgV);
        make.height.mas_equalTo(1);
    }];
    
    [self.backButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.equalTo(self.navBGImgV);
        make.top.equalTo(self.navBGImgV).offset(KStatusHeight);
        make.width.equalTo(self.backButton.mas_height);
    }];
    
    [self.rightButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.navBGImgV).offset(-10);
        make.bottom.equalTo(self.navBGImgV);
        make.top.equalTo(self.navBGImgV).offset(KStatusHeight);
        make.width.equalTo(@40);
    }];
    
    [self.navigationTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backButton.mas_right);
        make.top.equalTo(self.backButton);
        make.right.equalTo(self.rightButton.mas_left);
        make.bottom.equalTo(self.navBGImgV);
    }];
    
}


#pragma mark - ----------------- set

- (void)setNavTitleStr:(NSString *)navTitleStr {
    self.navigationTitleLabel.text = navTitleStr;
}


#pragma mark - ----------------- action

- (void)navBackClick {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)navRightClick {}

#pragma mark - ----------------- lazy

- (UIImageView *)navBGImgV {
    if (!_navBGImgV) {
        _navBGImgV = [[UIImageView alloc] initWithImage:[UIImage imageWithColor:Color_white]];
        _navBGImgV.userInteractionEnabled = YES;
    }
    return _navBGImgV;
}

- (UIView *)separatorView {
    if (!_separatorView) {
        _separatorView = [[UIView alloc] init];
        _separatorView.backgroundColor = Color_backgound;
    }
    return _separatorView;
}

-(UIButton *)backButton{
    if (!_backButton) {
        _backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_backButton setImage:[UIImage imageNamed:@"back_btn"] forState:UIControlStateNormal];
        [_backButton addTarget:self action:@selector(navBackClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _backButton;
}

-(UIButton *)rightButton{
    if (!_rightButton) {
        _rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_rightButton addTarget:self action:@selector(navRightClick) forControlEvents:UIControlEventTouchUpInside];
        [_rightButton setTitleColor:Color_title_333 forState:UIControlStateNormal];
        _rightButton.titleLabel.font = FONT_SIZE(16);
    }
    return _rightButton;
}

-(UILabel *)navigationTitleLabel{
    if (!_navigationTitleLabel) {
        _navigationTitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _navigationTitleLabel.font = FONT_SIZE(18);
        _navigationTitleLabel.textColor = Color_title_333;
        _navigationTitleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _navigationTitleLabel;
}



@end
