//
//  CBEBaseNavController.m
//  CBECommerce
//
//  Created by Eleven_Liu on 2020/5/18.
//  Copyright © 2020 Eleven_Liu. All rights reserved.
//

#import "CBEBaseNavController.h"

@interface CBEBaseNavController () <UINavigationControllerDelegate, UIGestureRecognizerDelegate>

@end

@implementation CBEBaseNavController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //隐藏导航栏背景
    [self setNavigationBarHidden:YES];
    
    WeakSelf(weakSelf)
    if ([self respondsToSelector:@selector(interactivePopGestureRecognizer)])
    {
        self.interactivePopGestureRecognizer.delegate = weakSelf;
        self.delegate = weakSelf;
    }
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if (self.childViewControllers.count > 0) { // 隐藏底部Bar
        viewController.hidesBottomBarWhenPushed = YES;
    }
    
    if ([self respondsToSelector:@selector(interactivePopGestureRecognizer)])
        self.interactivePopGestureRecognizer.enabled = NO;
    
    [super pushViewController:viewController animated:animated];
}


#pragma mark UINavigationControllerDelegate

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animate
{
    // 返回手势失效的controller
    NSArray *enablePopGestureViewControllers = @[];
    
    self.interactivePopGestureRecognizer.enabled = (self.viewControllers.count > 1)&&![enablePopGestureViewControllers containsObject:NSStringFromClass([viewController class])];
}




@end
