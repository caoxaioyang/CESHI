//
//  CBEBaseVC.h
//  CBECommerce
//
//  Created by Eleven_Liu on 2020/5/18.
//  Copyright © 2020 Eleven_Liu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBEBaseVC : UIViewController
//导航条背景图片
@property (nonatomic, strong) UIImageView *navBGImgV;
//分割线
@property (nonatomic, strong) UIView *separatorView;
//返回按钮
@property(nonatomic,strong) UIButton * backButton;
//导航栏标题
@property(nonatomic,strong) UILabel * navigationTitleLabel;
//导航栏右按钮
@property(nonatomic,strong) UIButton * rightButton;

//set
@property (nonatomic, strong) NSString *navTitleStr;

//返回按钮和右按钮点击方法，如果需要实现不同的方法，子类可以重新该方法
-(void)navBackClick;
-(void)navRightClick;


@end

NS_ASSUME_NONNULL_END
