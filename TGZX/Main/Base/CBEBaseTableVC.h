//
//  CBEBaseTableVC.h
//  CBECommerce
//
//  Created by Eleven_Liu on 2020/5/18.
//  Copyright © 2020 Eleven_Liu. All rights reserved.
//

#import "CBEBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBEBaseTableVC : CBEBaseVC <UITableViewDelegate, UITableViewDataSource>
@property (assign, nonatomic) UITableViewStyle tableViewStyle;//UITableView样式
@property (strong, nonatomic) UITableView *tableView;//UITableView
@property (nonatomic,assign) NSInteger currentPage;
@property (nonatomic,strong) NSMutableArray *dataSource;

-(void)setupTableView;

@end

NS_ASSUME_NONNULL_END
