//
//  CBEBaseAlertView.m
//  CBECommerce
//
//  Created by Eleven_Liu on 2020/5/18.
//  Copyright © 2020 Eleven_Liu. All rights reserved.
//

#import "CBEBaseAlertView.h"

#define SHOW_ANIMATION_DURATION 0.3

static BOOL isShowAction;//当前是否已弹出菜单(防止重复弹出)


@interface CBEBaseAlertView ()

@property (nonatomic, strong) UIView *maskBackgroundView;

@end


@implementation CBEBaseAlertView

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        
        // 半透明背景
        UIView *maskView = [CBECreateUITools createUIView:self.bounds backgroundColor:HEXCOLOR(0x000000) superView:self];
        maskView.alpha = 0.6;
        self.maskBackgroundView = maskView;
        
        [self setupSubViews];
        
        
    }
    return self;
}

//让子类实现
- (void)setupSubViews {}


#pragma mark - show remove

- (void)showInView:(UIView * _Nullable)view {
    if (isShowAction) {
        [self removeAlertView];
        return;
    }
    isShowAction = YES;
    if (view) {
        [view addSubview:self];
    }else {
        [[UIApplication sharedApplication].delegate.window addSubview:self];
    }
    [self showAinmation];
}

- (void)removeAlertView {
    [UIView animateWithDuration:SHOW_ANIMATION_DURATION animations:^{
        self.maskBackgroundView.alpha = 0;
        [self removeFromSuperview];
    }completion:^(BOOL finished) {
        isShowAction = NO;
    }];
}

- (void)showAinmation {
    self.maskBackgroundView.alpha = 0;
    [UIView animateWithDuration:SHOW_ANIMATION_DURATION animations:^{
        self.maskBackgroundView.alpha = 0.6;
    }];
}


@end
