//
//  CBEBaseLoadHtmlVC.h
//  CBECommerce
//
//  Created by Eleven_Liu on 2020/5/18.
//  Copyright © 2020 Eleven_Liu. All rights reserved.
//

#import "CBEBaseVC.h"
#import <WebKit/WebKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBEBaseLoadHtmlVC : CBEBaseVC <WKNavigationDelegate, WKUIDelegate>
@property(nonatomic,strong) WKWebView *webView;
@property (nonatomic, strong) UIProgressView *progressView;
@property (nonatomic, strong) UIBarButtonItem *backItem;
@property (nonatomic, strong) UIBarButtonItem *closeItem;

@property (nonatomic, copy) NSString *navTitle;
@property (nonatomic, copy) NSString *urlString;

@property (nonatomic, copy) NSString *openUrl;


@end

NS_ASSUME_NONNULL_END
