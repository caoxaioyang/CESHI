//
//  CBEBaseTableViewCell.h
//  CBECommerce
//
//  Created by Eleven_Liu on 2020/5/18.
//  Copyright © 2020 Eleven_Liu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CBEBaseTableViewCell : UITableViewCell

- (void)setup;

@end

NS_ASSUME_NONNULL_END
