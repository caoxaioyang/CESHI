//
//  AppDelegate.m
//  TGZX
//
//  Created by 曹晓阳 on 2021/10/8.
//

#import "AppDelegate.h"
#import "CBETabBarController.h"
#import "LoginViewController.h"
#import "CBEBaseNavController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    
    CBEBaseNavController *nav = [[CBEBaseNavController alloc] initWithRootViewController:[[LoginViewController alloc] init]];
    self.window.rootViewController = nav;
    [self.window makeKeyAndVisible];
    return YES;
}

@end
