//
//  StopViewController.h
//  TGZX
//
//  Created by 曹晓阳 on 2021/10/11.
//

#import "CBEBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface StopViewController : CBEBaseVC
@property (nonatomic, assign) BOOL isTabbarVC;

@end

NS_ASSUME_NONNULL_END
