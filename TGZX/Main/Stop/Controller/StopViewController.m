//
//  StopViewController.m
//  TGZX
//
//  Created by 曹晓阳 on 2021/10/11.
//

#import "StopViewController.h"
#import "ShopTableViewCell.h"
#import "ShoppingCartViewController.h"
#import "IntentionViewController.h"

@interface StopViewController ()<UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate>
@property (nonatomic, strong) UILabel *gwTag;

@property (nonatomic, strong) UIButton *orderBtn;
@property (nonatomic, strong) UIButton *rightBtn;

@property (nonatomic, strong) UITableView *tabView;

@property (nonatomic, strong) UIView *searchV;
@property (nonatomic ,strong) UITextField *textField;
@property (nonatomic, strong) UIButton *searchBtn;

@end

@implementation StopViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNav];
    [self setSearch];
    [self setTab];
}
- (void)setTab{
    self.tabView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    self.tabView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tabView.delegate = self;
    self.tabView.dataSource = self;
    self.tabView.showsVerticalScrollIndicator = NO;
    self.tabView.estimatedSectionHeaderHeight = 0;
    self.tabView.estimatedSectionFooterHeight = 0;
    [self.view addSubview:self.tabView];
    [self.tabView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.searchV.mas_bottom).offset(10);
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(-kTabBarHeight);
    }];
    [self.tabView registerNib:[UINib nibWithNibName:@"ShopTableViewCell" bundle:nil] forCellReuseIdentifier:@"stopCell"];

}
- (void)setSearch {
    self.searchV = [UIView new];
    self.searchV.backgroundColor = Color_white;
    [self.view addSubview:self.searchV];
    
    self.textField = [[UITextField alloc] init];
//    ViewRadius(topBgView, 5)
    [self.searchV addSubview:self.textField];
    self.textField.backgroundColor = Color_backgound;
    self.textField.returnKeyType = UIReturnKeyDone;
    self.textField.borderStyle = UITextBorderStyleRoundedRect;
    self.textField.font = FONT_SIZE(15);
    self.textField.textColor = [UIColor whiteColor];
    //左视图
    UIImage *searchImage = [UIImage imageNamed:@"attention_top_search"];
    UIView *leftView = [[UIView alloc]init];
    leftView.size = CGSizeMake(searchImage.size.width + 30, searchImage.size.height);
    UIImageView *leftImage = [[UIImageView alloc]initWithImage:searchImage];
    leftImage.frame = CGRectMake(15, 0, 15, 15);
    [leftView addSubview:leftImage];
    self.textField.leftViewMode = UITextFieldViewModeAlways;
    self.textField.leftView = leftView;
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:@"请输入关键字搜索" attributes: @{
        NSForegroundColorAttributeName:RGB(185, 181, 197),
        NSFontAttributeName:FONT_SIZE(15) }];
    self.textField.attributedPlaceholder = attrString;
    self.textField.delegate = self;
    [self.textField addTarget:self action:@selector(tfClick:) forControlEvents:UIControlEventEditingChanged];
    
    self.searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.searchBtn.backgroundColor = Color_clear;
    [self.searchBtn setTitleColor:RGB(42, 152, 255) forState:UIControlStateNormal];
    [self.searchBtn setTitle:@"搜索" forState:UIControlStateNormal];
    [self.searchBtn addTarget:self action:@selector(searchBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.searchV addSubview:self.searchBtn];
    
    [self.searchV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navBGImgV.mas_bottom).offset(10);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(60);
    }];
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(5);
        make.left.mas_equalTo(20);
        make.bottom.mas_equalTo(-5);
        make.right.mas_equalTo(-80);
    }];
    [self.searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.textField.mas_right).offset(3);
        make.top.bottom.mas_equalTo(0);
        make.right.mas_equalTo(-5);
    }];
}
- (void)setNav {
    WeakSelf(weakSelf);

    self.navigationTitleLabel.text = @"商城";
    self.separatorView.hidden = YES;
    self.rightButton.hidden = YES;

    self.rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.rightBtn setBackgroundImage:[UIImage imageNamed:@"stop_gw"] forState:UIControlStateNormal];
    [self.rightBtn addBlockForControlEvents:UIControlEventTouchUpInside block:^(id  _Nonnull sender) {
        [weakSelf navRightAction];
    }];
    [self.navBGImgV addSubview:self.rightBtn];
    
    self.gwTag = [CBECreateUITools createOneLabel:CGRectZero title:@"3" superView:self.navBGImgV font:FONT_SIZE(10) textColor:Color_white];
    self.gwTag.backgroundColor = Color_red;
    self.gwTag.textAlignment = NSTextAlignmentCenter;
    self.gwTag.layer.cornerRadius = 8;
    self.gwTag.layer.masksToBounds = YES;
    
    self.orderBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.orderBtn setBackgroundImage:[UIImage imageNamed:@"stop_dz"] forState:UIControlStateNormal];
    [self.orderBtn addBlockForControlEvents:UIControlEventTouchUpInside block:^(id  _Nonnull sender) {
        [weakSelf orderAction];
    }];
    [self.navBGImgV addSubview:self.orderBtn];
    
    [self.rightBtn mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.navBGImgV).offset(-20);
        make.bottom.equalTo(self.navBGImgV);
        make.width.equalTo(@30);
        make.height.equalTo(@36);

    }];
    
    [self.gwTag mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.rightBtn.mas_right).offset(-7);
        make.top.equalTo(self.rightBtn.mas_top).offset(0);
        make.height.width.mas_equalTo(16);
    }];
    [self.orderBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.rightBtn.mas_left).offset(-14);
        make.bottom.equalTo(self.navBGImgV).offset(-3);
        make.height.width.mas_equalTo(30);
    }];
}
- (void)orderAction {
    IntentionViewController *intentionVC = [IntentionViewController new];
    [self.navigationController pushViewController:intentionVC animated:NO];
}
- (void)navRightAction {
    ShoppingCartViewController *cartVC = [ShoppingCartViewController new];
    [self.navigationController pushViewController:cartVC animated:NO];
}
- (void)searchBtnAction:(UIButton *)btn {
    NSLog(@"搜索");
}
#pragma mark - tabDelegete
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 15;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"stopCell";
    ShopTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];

    if (cell == nil) {
        [tableView registerNib:[UINib nibWithNibName:@"ShopTableViewCell" bundle:nil] forCellReuseIdentifier:cellID];
        cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
  
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 145;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.001;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.001;
}
-(void)tfClick:(UITextField *)tf{
    if (tf.text.length == 0) {
       
    }else{
        
    }
}
@end
