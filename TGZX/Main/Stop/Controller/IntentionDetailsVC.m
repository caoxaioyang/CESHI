//
//  IntentionDetailsVC.m
//  TGZX
//
//  Created by 曹晓阳 on 2021/10/21.
//

#import "IntentionDetailsVC.h"

@interface IntentionDetailsVC ()

@end

@implementation IntentionDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNav];
    [self setHeaderView];
    [self detailView];
}
- (void)detailView {
    UIView *headerV = [CBECreateUITools createUIViewBackgroundColor:Color_white superView:self.view];
    headerV.layer.cornerRadius = 5;
    UILabel *left1 = [CBECreateUITools createOneLabel:@"零件号" superView:headerV font:FONT_SIZE(14) textColor:Color_text_666];
    UILabel *left2 = [CBECreateUITools createOneLabel:@"手册中文名称" superView:headerV font:FONT_SIZE(14) textColor:Color_text_666];
    UILabel *left3 = [CBECreateUITools createOneLabel:@"计量单位" superView:headerV font:FONT_SIZE(14) textColor:Color_text_666];
    UILabel *left4 = [CBECreateUITools createOneLabel:@"22710291" superView:headerV font:FONT_SIZE(16) textColor:Color_black];
    UILabel *left5 = [CBECreateUITools createOneLabel:@"插头和插座" superView:headerV font:FONT_SIZE(16) textColor:Color_black];
    UILabel *left6 = [CBECreateUITools createOneLabel:@"XXXX" superView:headerV font:FONT_SIZE(16) textColor:Color_black];
    
    UILabel *right1 = [CBECreateUITools createOneLabel:@"零件中文名称" superView:headerV font:FONT_SIZE(14) textColor:Color_text_666];
    UILabel *right2 = [CBECreateUITools createOneLabel:@"采购专员联系方式" superView:headerV font:FONT_SIZE(14) textColor:Color_text_666];
    UILabel *right3 = [CBECreateUITools createOneLabel:@"客服服务专员联系方式" superView:headerV font:FONT_SIZE(14) textColor:Color_text_666];
    UILabel *right4 = [CBECreateUITools createOneLabel:@"2021-01-01 12:00:00" superView:headerV font:FONT_SIZE(16) textColor:Color_black];
    UILabel *right5 = [CBECreateUITools createOneLabel:@"13012341234" superView:headerV font:FONT_SIZE(16) textColor:Color_black];
    UILabel *right6 = [CBECreateUITools createOneLabel:@"13012312311" superView:headerV font:FONT_SIZE(16) textColor:Color_black];
    
    [headerV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navBGImgV.mas_bottom).offset(240);
        make.left.mas_equalTo(20);
        make.right.mas_equalTo(-20);
        make.height.mas_equalTo(200);
    }];
    
    [right1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(20);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(SCREEN_WIDTH/2);
    }];
    [right4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(right1.mas_bottom).offset(3);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(SCREEN_WIDTH/2);
    }];
    [right2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(right4.mas_bottom).offset(15);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(SCREEN_WIDTH/2);
    }];
    [right5 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(right2.mas_bottom).offset(3);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(SCREEN_WIDTH/2);
    }];
    [right3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(right5.mas_bottom).offset(15);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(SCREEN_WIDTH/2);
    }];
    [right6 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(right3.mas_bottom).offset(3);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(SCREEN_WIDTH/2);
    }];
    
    [left1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(20);
        make.left.mas_equalTo(20);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(right1.mas_left);
    }];
    [left4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(left1.mas_bottom).offset(3);
        make.left.mas_equalTo(20);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(right4.mas_left);
    }];
    [left2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(left4.mas_bottom).offset(15);
        make.left.mas_equalTo(20);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(right2.mas_left);
    }];
    [left5 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(left2.mas_bottom).offset(3);
        make.left.mas_equalTo(20);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(right5.mas_left);
    }];
    [left3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(left5.mas_bottom).offset(15);
        make.left.mas_equalTo(20);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(right3.mas_left);
    }];
    [left6 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(left3.mas_bottom).offset(3);
        make.left.mas_equalTo(20);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(right6.mas_left);
    }];

}
- (void)setHeaderView {
    UIView *headerV = [CBECreateUITools createUIViewBackgroundColor:Color_white superView:self.view];
    
    UILabel *left1 = [CBECreateUITools createOneLabel:@"意向单号" superView:headerV font:FONT_SIZE(14) textColor:Color_text_666];
    UILabel *left2 = [CBECreateUITools createOneLabel:@"采购专员" superView:headerV font:FONT_SIZE(14) textColor:Color_text_666];
    UILabel *left3 = [CBECreateUITools createOneLabel:@"客服服务专员" superView:headerV font:FONT_SIZE(14) textColor:Color_text_666];
    UILabel *left4 = [CBECreateUITools createOneLabel:@"22710291" superView:headerV font:FONT_SIZE(16) textColor:Color_black];
    UILabel *left5 = [CBECreateUITools createOneLabel:@"张三" superView:headerV font:FONT_SIZE(16) textColor:Color_black];
    UILabel *left6 = [CBECreateUITools createOneLabel:@"李四" superView:headerV font:FONT_SIZE(16) textColor:Color_black];
    
    UILabel *right1 = [CBECreateUITools createOneLabel:@"下单时间" superView:headerV font:FONT_SIZE(14) textColor:Color_text_666];
    UILabel *right2 = [CBECreateUITools createOneLabel:@"采购专员联系方式" superView:headerV font:FONT_SIZE(14) textColor:Color_text_666];
    UILabel *right3 = [CBECreateUITools createOneLabel:@"客服服务专员联系方式" superView:headerV font:FONT_SIZE(14) textColor:Color_text_666];
    UILabel *right4 = [CBECreateUITools createOneLabel:@"2021-01-01 12:00:00" superView:headerV font:FONT_SIZE(16) textColor:Color_black];
    UILabel *right5 = [CBECreateUITools createOneLabel:@"13012341234" superView:headerV font:FONT_SIZE(16) textColor:Color_black];
    UILabel *right6 = [CBECreateUITools createOneLabel:@"13012312311" superView:headerV font:FONT_SIZE(16) textColor:Color_black];
    
    [headerV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navBGImgV.mas_bottom).offset(20);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(200);
    }];
    
    [right1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(20);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(SCREEN_WIDTH/2);
    }];
    [right4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(right1.mas_bottom).offset(3);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(SCREEN_WIDTH/2);
    }];
    [right2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(right4.mas_bottom).offset(15);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(SCREEN_WIDTH/2);
    }];
    [right5 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(right2.mas_bottom).offset(3);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(SCREEN_WIDTH/2);
    }];
    [right3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(right5.mas_bottom).offset(15);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(SCREEN_WIDTH/2);
    }];
    [right6 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(right3.mas_bottom).offset(3);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(SCREEN_WIDTH/2);
    }];
    
    [left1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(20);
        make.left.mas_equalTo(20);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(right1.mas_left);
    }];
    [left4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(left1.mas_bottom).offset(3);
        make.left.mas_equalTo(20);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(right4.mas_left);
    }];
    [left2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(left4.mas_bottom).offset(15);
        make.left.mas_equalTo(20);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(right2.mas_left);
    }];
    [left5 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(left2.mas_bottom).offset(3);
        make.left.mas_equalTo(20);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(right5.mas_left);
    }];
    [left3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(left5.mas_bottom).offset(15);
        make.left.mas_equalTo(20);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(right3.mas_left);
    }];
    [left6 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(left3.mas_bottom).offset(3);
        make.left.mas_equalTo(20);
        make.height.mas_equalTo(20);
        make.right.mas_equalTo(right6.mas_left);
    }];
    
}
- (void)setNav {
    self.navigationTitleLabel.text = @"意向详情";
}


@end
