//
//  ShoppingCartViewController.m
//  TGZX
//
//  Created by 曹晓阳 on 2021/10/19.
//

#import "ShoppingCartViewController.h"
#import "StopingCartTableViewCell.h"

@interface ShoppingCartViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UITableView *tabView;
@property (nonatomic, strong) UIButton *allSelectBtn;

@end

@implementation ShoppingCartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNav];
    [self setTab];
    [self setBottom];
}
- (void)setBottom {
    WeakSelf(weakSelf);
    UIView *bottomV = [CBECreateUITools createUIViewBackgroundColor:Color_white superView:self.view];
    UIView *line = [CBECreateUITools createUIViewBackgroundColor:Color_backgound superView:bottomV];
    self.allSelectBtn = [CBECreateUITools createExtendBoundUIButton:[UIImage imageNamed:@"orderSelN"] title:@"" backgroundColor:Color_clear edgeInsets:UIEdgeInsetsMake(-10, -10, -10, -10) superView:bottomV];
    [self.allSelectBtn addBlockForControlEvents:UIControlEventTouchUpInside block:^(id  _Nonnull sender) {
        [weakSelf allSelectBtnAction:sender];
    }];
    UILabel *allTitleLab = [CBECreateUITools createOneLabel:@"全选" superView:bottomV font:FONT_SIZE(14) textColor:Color_text_666];
    UIButton *submitBtn = [CBECreateUITools createUIButton:[UIImage imageNamed:@""] title:@"提交采购意向单" backgroundColor:RGB(130, 194, 253) superView:bottomV];
    submitBtn.layer.masksToBounds = YES;
    submitBtn.layer.cornerRadius = 5;
    [bottomV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
        make.height.mas_equalTo(kTabBarOffsetIphoneX+50);
    }];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(1);
    }];
    [submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(10);
        make.right.mas_equalTo(-15);
        make.left.mas_equalTo(70);
        make.height.mas_equalTo(40);
    }];
    [allTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(submitBtn.mas_left).offset(-5);
        make.centerY.mas_equalTo(submitBtn.mas_centerY);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(35);
    }];
    [self.allSelectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(allTitleLab.mas_left).offset(-5);
        make.centerY.mas_equalTo(submitBtn.mas_centerY);
        make.height.mas_equalTo(14);
        make.width.mas_equalTo(14);
    }];
    
    
}
- (void)setTab{
    self.tabView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    self.tabView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tabView.delegate = self;
    self.tabView.dataSource = self;
    self.tabView.showsVerticalScrollIndicator = NO;
    self.tabView.estimatedSectionHeaderHeight = 0;
    self.tabView.estimatedSectionFooterHeight = 0;
    [self.view addSubview:self.tabView];
    [self.tabView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kNavigaionBarHeight);
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(-kTabBarOffsetIphoneX-50);
    }];
//    [self.tabView registerNib:[UINib nibWithNibName:@"StopingCartTableViewCell" bundle:nil] forCellReuseIdentifier:@"StopingCartTableViewCell"];

}
- (void)setNav {
    self.navigationTitleLabel.text = @"购物车";
    [self.rightButton setImage:[UIImage imageNamed:@"stop_del"] forState:UIControlStateNormal];
}
- (void)allSelectBtnAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        [sender setImage:[UIImage imageNamed:@"orderSelY"] forState:UIControlStateNormal];
    }else{
        [sender setImage:[UIImage imageNamed:@"orderSelN"] forState:UIControlStateNormal];
    }
    [self.tabView reloadData];
}
//删除
-(void)navRightClick {
    
    [self.tabView reloadData];
}
#pragma mark - tabDelegete
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 15;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    StopingCartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"StopingCartTableViewCell"];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"StopingCartTableViewCell" owner:self options:nil] lastObject];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [cell.selBtn setTarget:self action:@selector(cellSelAction:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 145;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.001;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.001;
}
- (void)cellSelAction:(UIButton *)btn {
    NSLog(@"111");
    btn.selected = !btn.selected;
    if (btn.selected) {
        NSLog(@"111");
        [btn setImage:[UIImage imageNamed:@"orderSelY"] forState:UIControlStateNormal];
        
//        [self.selectedArray removeAllObjects];
//        self.selectedArray = [NSMutableArray arrayWithArray:self.dataArray];
//        for (int i = 0; i <self.dataArray.count; i ++) {
//            CBEMyCollectionModel *model = self.dataArray[i];
//            model.isSelect = 1;
//        }
    }else{
        NSLog(@"2222");

        [btn setImage:[UIImage imageNamed:@"orderSelN"] forState:UIControlStateNormal];
//        [self.selectedArray removeAllObjects];
//        for (int i = 0; i <self.dataArray.count; i ++) {
//            CBEMyCollectionModel *model = self.dataArray[i];
//            model.isSelect = 0;
//        }
    }
    [self.tabView reloadData];
}
@end
