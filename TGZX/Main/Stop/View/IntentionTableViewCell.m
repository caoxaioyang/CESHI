//
//  IntentionTableViewCell.m
//  TGZX
//
//  Created by 曹晓阳 on 2021/10/20.
//

#import "IntentionTableViewCell.h"

@implementation IntentionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setup{
    
    self.numb = [CBECreateUITools createOneLabel:@"意向单号：22710291" superView:self.contentView font:BOLD_SIZE(14) textColor:Color_black];
    self.time = [CBECreateUITools createOneLabel:@"下单时间：2021-01-01 12：00：00" superView:self.contentView font:FONT_SIZE(14) textColor:Color_text_666];
    self.purchaseName = [CBECreateUITools createOneLabel:@"采购专员：张三" superView:self.contentView font:FONT_SIZE(14) textColor:Color_text_666];
    self.serviceName = [CBECreateUITools createOneLabel:@"客户服务专员：李四" superView:self.contentView font:FONT_SIZE(14) textColor:Color_text_666];
    self.img = [CBECreateUITools createImageView:CGRectZero image:[UIImage imageNamed:@"arrow_img_right"] superView:self.contentView];
    UIView *line = [CBECreateUITools createUIViewBackgroundColor:Color_backgound superView:self.contentView];
    [self.numb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.top.mas_equalTo(10);
        make.height.mas_equalTo(20);
    }];
    [self.img mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-20);
        make.centerY.mas_equalTo(self.numb.centerY);
        make.width.mas_equalTo(5);
        make.height.mas_equalTo(9);
    }];
    [self.time mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.top.equalTo(self.numb.mas_bottom).offset(5);
        make.height.mas_equalTo(20);
    }];
    [self.purchaseName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.top.equalTo(self.time.mas_bottom).offset(5);
        make.height.mas_equalTo(20);
    }];
    [self.serviceName mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.top.equalTo(self.purchaseName.mas_bottom).offset(5);
        make.height.mas_equalTo(20);
    }];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.right.mas_equalTo(-20);
        make.height.mas_equalTo(1);
        make.bottom.mas_equalTo(0);
    }];
}

@end
