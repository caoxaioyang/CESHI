//
//  IntentionTableViewCell.h
//  TGZX
//
//  Created by 曹晓阳 on 2021/10/20.
//

#import "CBEBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface IntentionTableViewCell : CBEBaseTableViewCell
@property (nonatomic, strong) UILabel *numb;
@property (nonatomic, strong) UILabel *time;
@property (nonatomic, strong) UILabel *purchaseName;
@property (nonatomic, strong) UILabel *serviceName;
@property (nonatomic, strong) UIImageView *img;

@end

NS_ASSUME_NONNULL_END
