//
//  StopingCartTableViewCell.h
//  TGZX
//
//  Created by 曹晓阳 on 2021/10/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface StopingCartTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *moneyTypeBtn;
@property (weak, nonatomic) IBOutlet CBEExtendBoundButton *selBtn;

@end

NS_ASSUME_NONNULL_END
