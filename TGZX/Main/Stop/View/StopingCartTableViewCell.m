//
//  StopingCartTableViewCell.m
//  TGZX
//
//  Created by 曹晓阳 on 2021/10/19.
//

#import "StopingCartTableViewCell.h"

@implementation StopingCartTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.moneyTypeBtn.layer.masksToBounds = YES;
    self.moneyTypeBtn.layer.cornerRadius = 3;
    self.moneyTypeBtn.layer.borderWidth = 1;
    self.moneyTypeBtn.layer.borderColor = Color_backgound.CGColor;
    
    self.selBtn.hitTestEdgeInsets = UIEdgeInsetsMake(-10, -10, -10, -10);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
