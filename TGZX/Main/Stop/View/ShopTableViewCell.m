//
//  ShopTableViewCell.m
//  TGZX
//
//  Created by 曹晓阳 on 2021/10/15.
//

#import "ShopTableViewCell.h"

@implementation ShopTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.moneyTypeBtn.layer.masksToBounds = YES;
    self.moneyTypeBtn.layer.cornerRadius = 3;
    self.moneyTypeBtn.layer.borderWidth = 1;
    self.moneyTypeBtn.layer.borderColor = Color_backgound.CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
