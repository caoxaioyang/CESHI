//
//  ShopTableViewCell.h
//  TGZX
//
//  Created by 曹晓阳 on 2021/10/15.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ShopTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *moneyTypeBtn;
@property (weak, nonatomic) IBOutlet UIImageView *iconImg;
@property (weak, nonatomic) IBOutlet UIImageView *smallImg;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *discountLab;
@property (weak, nonatomic) IBOutlet UILabel *priceLab;

@property (weak, nonatomic) IBOutlet UIButton *carBtn;
@end

NS_ASSUME_NONNULL_END
