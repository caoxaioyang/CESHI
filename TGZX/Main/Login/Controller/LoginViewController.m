//
//  LoginViewController.m
//  TGZX
//
//  Created by 曹晓阳 on 2021/10/11.
//

#import "LoginViewController.h"
#import "YYTimer.h"
#import "YYText.h"
#import "CBEInputVerifyTool.h"
#import "CBELoadHtmlVC.h"
#import "RegisterVC.h"
#import "CBETabBarController.h"

//#import "CBELoginModel.h"
@interface LoginViewController ()
@property (nonatomic, strong) UITextField *mobileTextField;
@property (nonatomic, strong) UITextField *codeTextField;
@property (nonatomic, strong) UIButton *codeBtn;
@property (nonatomic, strong) UIButton *loginBtn;

@property (nonatomic, assign) int countTime;
@property (nonatomic, strong) YYTimer *timer;


@property (nonatomic, strong) UILabel *recordPwd;
@property (nonatomic, strong) UIButton *recordPwdBtn;
@property (nonatomic, strong) UIButton *ruleBtn;
@property (nonatomic, strong) UIButton *loginTypeBtn;

@property (nonatomic) BOOL type;
@property (nonatomic) BOOL pwdType;
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = Color_white;
    self.type = NO;
    self.pwdType = NO;
    [self setupSubViews];

}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
}
-(void)viewWillAppear:(BOOL)animated {
}

- (void)setupSubViews {

    NSString *titleStr = self.isMobile ? @"手机号":@"用户名";
    NSString *btnTitleStr = self.isMobile ? @"验证码":@"密码";
    
    
    UIImageView *headerImg = [CBECreateUITools createImageView:[UIImage imageNamed:@"login_header"] superView:self.view];
    
    UILabel *phoneLab = [CBECreateUITools createOneLabel:titleStr superView:self.view font:FONT_SIZE(14) textColor:Color_title_333 textAlignment:NSTextAlignmentCenter];
    
    UIView *lineView = [CBECreateUITools createUIViewBackgroundColor:Color_line superView:self.view];
    
    self.mobileTextField = [[UITextField alloc] init];
    self.mobileTextField.font = FONT_SIZE(14);
    self.mobileTextField.textColor = Color_title_333;
    self.mobileTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.mobileTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.mobileTextField.placeholder = self.isMobile ? @"请输入手机号":@"请输入用户名";
    [self.mobileTextField addTarget:self action:@selector(accountTextFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.view addSubview:self.mobileTextField];
    
    UILabel *codeLab = [CBECreateUITools createOneLabel:btnTitleStr superView:self.view font:FONT_SIZE(14) textColor:Color_title_333 textAlignment:NSTextAlignmentCenter];
    
    UIView *lineView2 = [CBECreateUITools createUIViewBackgroundColor:Color_line superView:self.view];
       
    self.codeTextField = [[UITextField alloc] init];
    self.codeTextField.font = FONT_SIZE(14);
    self.codeTextField.textColor = Color_title_333;
//    self.codeTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.codeTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.codeTextField.placeholder = self.isMobile ? @"请输入验证码":@"请输入密码";
    self.codeTextField.secureTextEntry = YES;
    [self.codeTextField addTarget:self action:@selector(codeTextFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.view addSubview:self.codeTextField];
    
    WeakSelf(weakSelf);
    UIButton *codeBtn = [CBECreateUITools createUIButton:CGRectZero image:nil title:@"" backgroundColor:nil superView:self.view];
    codeBtn.titleLabel.font = FONT_SIZE(13);
    [codeBtn setTitleColor:Color_text_999 forState:UIControlStateNormal];
    [codeBtn setBackgroundImage:[UIImage imageNamed:@"pwdB"] forState:UIControlStateNormal];
    self.codeBtn = codeBtn;
    [codeBtn addBlockForControlEvents:UIControlEventTouchUpInside block:^(id  _Nonnull sender) {
        if (self.type == 1) {
            UIButton *btn = sender;
            [weakSelf getSecurityCodeAction:btn];
        } else {
            [weakSelf changePwdType];
        }
      
    }];
    
    self.loginBtn = [CBECreateUITools createUIButton:CGRectZero image:nil title:@"登录" backgroundColor:RGB(130, 194, 253) superView:self.view];
    self.loginBtn.titleLabel.font = FONT_SIZE(15);
    self.loginBtn.layer.cornerRadius = 5;
    [self.loginBtn addBlockForControlEvents:UIControlEventTouchUpInside block:^(id  _Nonnull sender) {
        [weakSelf loginAction];
    }];
    
    self.ruleBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [self.ruleBtn setBackgroundImage:[UIImage imageNamed:@"fangk"] forState:UIControlStateNormal];
    self.ruleBtn.backgroundColor = Color_white;
    self.ruleBtn.tag = 0;
    [self.ruleBtn addBlockForControlEvents:UIControlEventTouchUpInside block:^(id  _Nonnull sender) {
        if (weakSelf.ruleBtn.tag == 0) {
            [weakSelf.ruleBtn setBackgroundImage:[UIImage imageNamed:@"duig"] forState:UIControlStateNormal];
            weakSelf.ruleBtn.tag = 1;
        } else {
            [weakSelf.ruleBtn setBackgroundImage:[UIImage imageNamed:@"fangk"] forState:UIControlStateNormal];
            weakSelf.ruleBtn.tag = 0;
        }
    }];
    [self.view addSubview:self.ruleBtn];
    
    NSString *str = @"我已阅读并完全接受《服务条款》";
    NSMutableAttributedString *string2 = [[NSMutableAttributedString alloc] initWithString:str attributes:@{NSForegroundColorAttributeName: Color_text_666}];
    
    NSRange range = [str rangeOfString:@"《服务条款》"];
    [string2 addAttributes:@{NSForegroundColorAttributeName: RGB(130, 194, 253)} range:range];
    
    //标记文字点击事件
    [string2 yy_setTextHighlightRange:range color:RGB(130, 194, 253) backgroundColor:nil tapAction:^(UIView * _Nonnull containerView, NSAttributedString * _Nonnull text, NSRange range, CGRect rect) {
        CBELoadHtmlVC *html = [[CBELoadHtmlVC alloc] init];
        NSURL *url = [[NSBundle mainBundle] URLForResource:@"read.html" withExtension:nil];
        html.urlString = url.absoluteString;
        [weakSelf.navigationController pushViewController:html animated:YES];
    }];
    
    YYLabel *ruleLab = [[YYLabel alloc] init];
    ruleLab.attributedText = string2;
    ruleLab.font = FONT_SIZE(14);
    // YYLabel要想自动换行，必须设置最大换行的宽度
//    ruleLab.preferredMaxLayoutWidth = SCREEN_WIDTH-2*34-40;
//    ruleLab.numberOfLines = 0;
    ruleLab.backgroundColor = [UIColor clearColor];
    ruleLab.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:ruleLab];
    
    self.recordPwdBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [self.recordPwdBtn setBackgroundImage:[UIImage imageNamed:@"fangk"] forState:UIControlStateNormal];
    self.recordPwdBtn.backgroundColor = Color_white;
    self.recordPwdBtn.tag = 0;

    [self.view addSubview:self.recordPwdBtn];
    [self.recordPwdBtn addBlockForControlEvents:UIControlEventTouchUpInside block:^(id  _Nonnull sender) {
        if (weakSelf.recordPwdBtn.tag == 0) {
            [weakSelf.recordPwdBtn setBackgroundImage:[UIImage imageNamed:@"duig"] forState:UIControlStateNormal];
            weakSelf.recordPwdBtn.tag = 1;
        } else {
            [weakSelf.recordPwdBtn setBackgroundImage:[UIImage imageNamed:@"fangk"] forState:UIControlStateNormal];
            weakSelf.recordPwdBtn.tag = 0;
        }
    }];
    self.recordPwd = [CBECreateUITools createOneLabel:@"记住用户名和密码" superView:self.view font:FONT_SIZE(14) textColor:Color_text_666 textAlignment:NSTextAlignmentCenter];
    
    UIButton *registerBtn = [CBECreateUITools createUIButton:CGRectZero image:nil title:@"账号注册" backgroundColor:Color_clear superView:self.view];
    [registerBtn setTitleColor:Color_text_666 forState:UIControlStateNormal];
    registerBtn.titleLabel.font = FONT_SIZE(15);
    [registerBtn addBlockForControlEvents:UIControlEventTouchUpInside block:^(id  _Nonnull sender) {
        [weakSelf registerAction];
    }];
    
    UIView *lineView3 = [CBECreateUITools createUIViewBackgroundColor:Color_line superView:self.view];

    
    self.loginTypeBtn = [CBECreateUITools createUIButton:CGRectZero image:nil title:@"验证码登录" backgroundColor:Color_clear superView:self.view];
    [self.loginTypeBtn setTitleColor:Color_text_666 forState:UIControlStateNormal];
    self.loginTypeBtn.titleLabel.font = FONT_SIZE(15);
    [self.loginTypeBtn addBlockForControlEvents:UIControlEventTouchUpInside block:^(id  _Nonnull sender) {
        [weakSelf loginTypeAction];
    }];
    
    
    [headerImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(0);
        make.height.mas_equalTo(kScreenWidthRatio*250);
    }];
    [phoneLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(22);
        make.top.equalTo(headerImg.mas_bottom).offset(20);
        make.height.mas_equalTo(25);
    }];
    [self.mobileTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(22);
        make.top.equalTo(phoneLab.mas_bottom).offset(5);
        make.height.mas_equalTo(30);
        make.right.mas_equalTo(-22);

    }];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(22);
        make.top.equalTo(self.mobileTextField.mas_bottom).offset(3);
        make.right.mas_equalTo(-22);
        make.height.mas_equalTo(1);
    }];
    [codeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lineView.mas_bottom).offset(25);
        make.left.mas_equalTo(22);
        make.height.mas_equalTo(20);
    }];
    [self.codeTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(22);
        make.top.equalTo(codeLab.mas_bottom).offset(5);
        make.height.mas_equalTo(30);
        make.right.mas_equalTo(-22);
    }];
    [self.codeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-22);
        make.height.mas_equalTo(8);
        make.width.mas_equalTo(16);
        make.centerY.equalTo(self.codeTextField.mas_centerY).offset(0);
    }];
    [lineView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(22);
        make.top.equalTo(self.codeTextField.mas_bottom).offset(3);
        make.right.mas_equalTo(-22);
        make.height.mas_equalTo(1);
    }];
    
    [self.ruleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(22);
        make.top.equalTo(lineView2.mas_bottom).offset(20);
        make.height.width.mas_equalTo(17);
    }];
    [ruleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.ruleBtn.mas_right).offset(15);
        make.top.equalTo(lineView2.mas_bottom).offset(20);
        make.height.mas_equalTo(15);
    }];
    [self.recordPwdBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(22);
        make.top.equalTo(self.ruleBtn.mas_bottom).offset(20);
        make.height.width.mas_equalTo(17);
    }];
    [self.recordPwd mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.recordPwdBtn.mas_right).offset(15);
        make.top.equalTo(self.ruleBtn.mas_bottom).offset(20);
        make.height.mas_equalTo(17);
    }];
    [self.loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.recordPwd.mas_bottom).offset(35);
        make.left.mas_equalTo(22);
        make.right.mas_equalTo(-22);
        make.height.mas_equalTo(45);
    }];
    [lineView3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.loginBtn.mas_bottom).offset(20);
        make.centerX.mas_equalTo(0);
        make.width.mas_equalTo(1);
        make.height.mas_equalTo(10);
    }];

    [registerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.right.equalTo(lineView3.mas_left).offset(-10);
        make.centerY.equalTo(lineView3.mas_centerY).offset(0);
    }];
    [self.loginTypeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(20);
        make.centerY.equalTo(lineView3.mas_centerY).offset(0);
        make.left.equalTo(lineView3.mas_right).offset(10);
    }];
}
- (void)changePwdType {
    if (self.pwdType) {
        self.codeTextField.secureTextEntry = YES;
        self.pwdType = NO;
        [self.codeBtn setBackgroundImage:[UIImage imageNamed:@"pwdB"] forState:UIControlStateNormal];
        NSLog(@"2");

    } else {
        NSLog(@"1");
        self.codeTextField.secureTextEntry = NO;
        self.pwdType = YES;
        [self.codeBtn setBackgroundImage:[UIImage imageNamed:@"pwdK"] forState:UIControlStateNormal];
    }
}
#pragma mark - 获取验证码
- (void)getSecurityCodeAction:(UIButton *)sender {
    
    NSString *phone = self.mobileTextField.text;
    
    if (EmptyStr(phone)) {
        [MBProgressHUD showMessageWithText:@"请输入手机号"];
        return;
    }
    if ([CBEInputVerifyTool checkMobileNumber:phone]) {
        [MBProgressHUD showMessageWithText:@"请输入正确的手机号"];
        return;
    }
    
    self.countTime = 60;
    self.codeBtn.enabled = NO;
    
    [self.codeBtn setTitleColor:HEXCOLOR(0xC19764) forState:UIControlStateNormal];
    
//    NSDictionary *params = @{@"mobile":phone};
    
//    [CBENetworkClass postWithHostIp:HostIp UrlString:GetRegisterCode parameters:params complish:^(id  _Nonnull responseObject, NSError * _Nonnull error) {
//
//        [MBProgressHUD showMessageWithText:responseObject[@"message"]];
//
//        if ([StrFmt(@"%@", responseObject[@"code"]) isEqualToString:@"20000"]) {
//
//            NSString *codeStr = StrFmt(@"%ds后重新获取", self.countTime);
//            [self.codeBtn setTitle:codeStr forState:UIControlStateNormal];
//            self.timer = [YYTimer timerWithTimeInterval:1 target:self selector:@selector(smsCode) repeats:YES];
//
//            [self.codeTextField becomeFirstResponder];
//        }else {
//
//            self.codeBtn.enabled = YES;
//        }
//    }];
}

- (void)smsCode {
    
    [self.codeBtn setTitle:[NSString stringWithFormat:@"%ds后重新获取", --self.countTime] forState:UIControlStateNormal];
    if (self.countTime <= 0) {
        [_timer invalidate];
        _timer = nil;
        self.codeBtn.enabled = YES;
        [self.codeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        [self.codeBtn setTitleColor:Color_title_333 forState:UIControlStateNormal];
    }
}


#pragma mark - ----------------- Login
- (void)loginTypeAction {
    
}
- (void)registerAction {
    RegisterVC *registerVC = [[RegisterVC alloc] initWithNibName:@"RegisterVC" bundle:nil];
    
    [registerVC.navigationController setNavigationBarHidden:YES];
    [self.navigationController pushViewController:registerVC animated:NO];
}
- (void)loginAction {
    
    [self.view endEditing:YES];//收起键盘
    
    [UIApplication sharedApplication].keyWindow.rootViewController = [CBETabBarController new];
    
//    NSString *mobile = self.mobileTextField.text;
//    NSString *code = self.codeTextField.text;
//    if (self.ruleBtn.tag == 0) {
//        [MBProgressHUD showMessageWithText:@"请接受服务条款"];
//        return;
//    }
//    if (EmptyStr(mobile)) {
//        [MBProgressHUD showMessageWithText:@"请输入手机号"];
//        return;
//    }
//    if ([CBEInputVerifyTool checkMobileNumber:mobile]) {
//        [MBProgressHUD showMessageWithText:@"请输入正确的手机号"];
//        return;
//    }
//
//    if (EmptyStr(code)) {
//        [MBProgressHUD showMessageWithText:@"请输入验证码"];
//        return;
//    }
//    if (code.length != 6) {
//        [MBProgressHUD showMessageWithText:@"验证码不正确"];
//        return;
//    }
//
//    //手机登录 status:1 手机号登录 2微信登录 3支付宝登录 4抖音登录
//    NSDictionary *params = @{@"status" : @1,
//                             @"mobile" : mobile,
//                             @"code"   : code};
//
//    NSString *urlStr = self.isMobile ? BindMoileApi:LoginApi;
//
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    [CBELoginModel postWithHostIp:HostIp UrlString:urlStr parameters:params isCache:NO complish:^(id  _Nonnull responseObject, NSString * _Nonnull errMsg, NSError * _Nonnull error) {
//        [MBProgressHUD hideHUDForView:self.view animated:YES];
//
//        CBELoginModel *model = responseObject;
//
//        [MBProgressHUD showMessageWithText:model.message];
//
//        if ([model.code isEqualToNumber:@20000]) {
//
//            CBEUserInfoModel *userInfo = model.data;
//            //更新数据
//            [CBEUserInfoManager shared].userInfoModel = userInfo;
//            [CBEUserInfoManager updateUserInfo];
//
//            if ([userInfo.token isKindOfClass:NSString.class]) {
//                [YYKeychain setPassword:userInfo.token forService:kTokenServiceName account:kTokenAccountName];
//            }
//
//            [self.navigationController popToRootViewControllerAnimated:YES];
//
//        }
//    }];
    
}


#pragma mark - ----------------- 监听输入的电话格式、监听输入的code格式

- (void)accountTextFieldDidChange:(UITextField *)textField {
//    if (textField.text.length > 11) {
//        textField.text = [textField.text substringToIndex:11];
//    }
}

- (void)codeTextFieldDidChange:(UITextField *)textField {
//    if (textField.text.length > 6) {
//        textField.text = [textField.text substringToIndex:6];
//    }
}
@end
