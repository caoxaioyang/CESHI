//
//  RegisterVC.m
//  TGZX
//
//  Created by 曹晓阳 on 2021/10/13.
//

#import "RegisterVC.h"
#import "YYTimer.h"
#import "CBEInputVerifyTool.h"

@interface RegisterVC ()<UIScrollViewDelegate>
//@property (nonatomic, strong) YYTimer *timer;
@property (nonatomic, assign) int countTime;
@property (weak, nonatomic) IBOutlet UIView *scrollV;
@property (nonatomic, strong) UIScrollView *scroll;
@property (strong, nonatomic) IBOutlet UIView *contentV;

@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *textFArr;

@property (weak, nonatomic) IBOutlet UIButton *codeBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintH;

@end

@implementation RegisterVC

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view from its nib.
    
    self.codeBtn.titleLabel.font = FONT_SIZE(11);
    self.constraintH.constant = kNavigaionBarHeight;
    [self setView];
}

- (void)setView {
    self.scroll = [[UIScrollView alloc] init];
    self.scroll.delegate = self;
    self.contentV.frame = CGRectMake(0, 0, SCREEN_WIDTH, 1300);
    self.scroll.contentSize = CGSizeMake(SCREEN_WIDTH, 1300);
    [self.scroll addSubview:self.contentV];
    [self.scrollV addSubview:self.scroll];
    [self.scroll mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.mas_equalTo(0);
    }];
}


- (IBAction)getCode:(UIButton *)sender {
}

- (IBAction)sexAction:(UIControl *)sender {
    [BRStringPickerView showPickerWithTitle:@"性别" dataSourceArr:@[@"男",@"女"] selectIndex:0 resultBlock:^(BRResultModel * _Nullable resultModel) {
        UITextField *textF = self.textFArr[5];
        textF.text = resultModel.value;
    }];
}
- (IBAction)birthdayAction:(UIControl *)sender {
    [BRDatePickerView showDatePickerWithMode:BRDatePickerModeYMD title:@"生日" selectValue:@"" resultBlock:^(NSDate * _Nullable selectDate, NSString * _Nullable selectValue) {
        UITextField *textF = self.textFArr[8];
        textF.text = selectValue;
    }];
}
- (IBAction)registerAction:(UIButton *)sender {
    
}
- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}

@end
