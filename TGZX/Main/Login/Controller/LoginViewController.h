//
//  LoginViewController.h
//  TGZX
//
//  Created by 曹晓阳 on 2021/10/11.
//

#import "CBEBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface LoginViewController : CBEBaseVC
@property (nonatomic, assign) BOOL isMobile; //是否手机号登录

@end

NS_ASSUME_NONNULL_END
