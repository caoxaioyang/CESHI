//
//  CBETabBarController.m
//  CBECommerce
//
//  Created by Eleven_Liu on 2020/5/18.
//  Copyright © 2020 Eleven_Liu. All rights reserved.
//

#import "CBETabBarController.h"
#import "CBEBaseNavController.h"
#import "CBEBaseVC.h"

#import "HomeViewController.h"
#import "StopViewController.h"
#import "MyViewController.h"
#import "CarViewController.h"
#import "NewsViewController.h"
#import "LoginViewController.h"

@interface CBETabBarController ()

@end

@implementation CBETabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSArray *controllers = @[HomeViewController.class,
                             StopViewController.class,
                             CarViewController.class,
                             NewsViewController.class,
                             MyViewController.class];
    NSArray *images = @[@"tabBar_Home",
                        @"tabBar_stop",
                        @"tabBar_car",
                        @"tabBar_news",
                        @"tabBar_my"];
    NSArray *titles = @[@"首页",@"商城",@"车辆",@"消息",@"我的"];
    
    for (NSInteger i = 0; i < controllers.count; i ++) {
        
        NSString *image = images[i];
        NSString *title = titles[i];
        
        CBEBaseVC *vc = [[controllers[i] alloc]init];
        if (i == 1) {
            StopViewController *vc1 = (StopViewController *)vc;
            vc1.isTabbarVC = YES;
        }
        CBEBaseNavController *nav = [self childViewControllerWithChildVC:vc title:title imageName:image];
        [self addChildViewController:nav];
    }
    self.tabBar.backgroundColor = Color_white;
}

- (CBEBaseNavController *)childViewControllerWithChildVC:(CBEBaseVC *)childVC title:(NSString *)title imageName:(NSString *)imgName {
    
    childVC.backButton.hidden = YES;
    childVC.view.backgroundColor =  Color_white;
    childVC.tabBarItem.title = title;
    childVC.tabBarItem.image = IMAGE_NAME(imgName);
    childVC.tabBarItem.selectedImage = IMAGE_NAME(StrFmt(@"%@_sel", imgName));
    
    CBEBaseNavController *nav = [[CBEBaseNavController alloc] initWithRootViewController:childVC];
    return nav;
}




@end
