//
//  CBEBaseModel.h
//  CBECommerce
//
//  Created by jifan on 2020/5/19.
//  Copyright © 2020 Eleven_Liu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MJExtension.h>
#import "CBENetworkClass.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, AFRequestType) {
    AFRequestTypeGET = 0,
    AFRequestTypePOST,
};

@interface CBEBaseModel : NSObject

+ (NSArray <NSString *>*)setupDataFieldsIsModelArray:(BOOL *)isModelArray;

+ (void)getWithHostIp:(NSString *)hostIp
            UrlString:(NSString *)URLString
           parameters:(NSDictionary *)parameters
              isCache:(BOOL)isCache
             complish:(void (^)(id responseObject, NSString *errMsg, NSError *error))complish;

+ (void)postWithHostIp:(NSString *)hostIp
             UrlString:(NSString *)URLString
            parameters:(NSDictionary *)parameters
               isCache:(BOOL)isCache
              complish:(void (^)(id responseObject, NSString *errMsg, NSError *error))complish;

@end

NS_ASSUME_NONNULL_END
