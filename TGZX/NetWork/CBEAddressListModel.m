//
//  CBEAddressListModel.m
//  CBECommerce
//
//  Created by jifan on 2020/5/19.
//  Copyright © 2020 Eleven_Liu. All rights reserved.
//

#import "CBEAddressListModel.h"

@implementation CBEAddressListDetailModel

+(NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{@"addressId":@"id"};
}

@end



@implementation CBEAddressListModel

+ (NSArray<NSString *> *)setupDataFieldsIsModelArray:(BOOL *)isModelArray{
    return @[@"data"];
}

+ (NSDictionary *)mj_objectClassInArray {
    return @{@"list" : [CBEAddressListDetailModel class]};
}

@end
