//
//  CBENetworkClass.m
//  CBECommerce
//
//  Created by jifan on 2020/5/19.
//  Copyright © 2020 Eleven_Liu. All rights reserved.
//

#import "CBENetworkClass.h"
#import <Reachability.h>

static Reachability *_reach;

@interface CBENetworkClass ()
@property (nonatomic, strong) NSString *baseUrl;
@end

@implementation CBENetworkClass

+ (instancetype)shared {
    static CBENetworkClass *net = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        net = [[CBENetworkClass alloc] initWithBaseURL:[NSURL URLWithString:HostIp]];
        
        //设置安全策略为client端不用安装证书和不用验证域名
        AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
        [securityPolicy setValidatesDomainName:NO];
        net.securityPolicy = securityPolicy;
        
        net.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain",@"video/mpeg",@"video/mp4",@"audio/mp3", nil];
        //设置请求headers
        net.requestSerializer = [AFHTTPRequestSerializer serializer];
        
        net.requestSerializer.timeoutInterval = 15;
        [net.requestSerializer setValue:@"iOS" forHTTPHeaderField:@"mobile-type"];
        [net.requestSerializer setValue:SYSTEM_VERSION forHTTPHeaderField:@"app-version"];
    });
    return net;
}

- (NSURL *)baseURL {
    return [NSURL URLWithString:self.baseUrl];
}


+ (BOOL)hasNetwork {
    return [[CBENetworkClass shared] hasNetwork];
}

- (BOOL)hasNetwork{
    _reach = [Reachability reachabilityWithHostname:@"www.baidu.com"];
    [_reach startNotifier];
    return _reach.isReachable;
}


+ (void)getWithHostIp:(NSString *)hostIp
            UrlString:(NSString *)URLString
           parameters:(NSDictionary *)parameters
             complish:(void (^)(id responseObject, NSError *error))complish
{
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:parameters];

    //token
    NSString *token = [YYKeychain getPasswordForService:kTokenServiceName account:kTokenAccountName];
    if (!EmptyStr(token)) {
        [params setObject:token forKey:@"token"];
    }
    //..
    
    
    
    __block NSString *paramsStr = @"";
    [params enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        paramsStr = [paramsStr stringByAppendingFormat:@"&%@=%@", key, obj];
    }];
    
    NSLog(@"get👉  %@%@?%@ \n", hostIp, URLString, paramsStr);
    
    CBENetworkClass *netClass = [CBENetworkClass shared];
    [netClass setBaseUrl:hostIp];
    
    
    [netClass GET:URLString parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *codeStr = StrFmt(@"%@", responseObject[@"code"]);
        if ([codeStr isEqualToString:@"20005"]) {
            //token过期，被挤下线
            [MBProgressHUD showMessageWithText:@"请重新登录"];
//            [JFUserInfoManager logoutComplish:^(id result, NSError * error) {}];
            return ;
        }
        complish(responseObject, nil);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complish(nil, error);
        [MBProgressHUD hideLoading];//一定要写在前面，有的界面有loading，先停止loading再显示下面的文字
    }];
}


+ (void)postWithHostIp:(NSString *)hostIp
             UrlString:(NSString *)URLString
            parameters:(NSDictionary *)parameters
              complish:(void (^)(id responseObject, NSError *error))complish
{
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:parameters];
    
    //token
    NSString *token = [YYKeychain getPasswordForService:kTokenServiceName account:kTokenAccountName];
    if (!EmptyStr(token)) {
        [params setObject:token forKey:@"token"];
    }

    __block NSString *paramsStr = @"";
    [params enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        paramsStr = [paramsStr stringByAppendingFormat:@"&%@=%@", key, obj];
    }];
    NSLog(@"post👉  %@%@?%@\n", hostIp, URLString, paramsStr);
    
    CBENetworkClass *netClass = [CBENetworkClass shared];
    [netClass setBaseUrl:hostIp];
    
    [netClass POST:URLString parameters:params headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString *codeStr = StrFmt(@"%@", responseObject[@"code"]);
        if ([codeStr isEqualToString:@"20005"]) {
            //token过期，被挤下线
            [MBProgressHUD showMessageWithText:@"请重新登录"];
//            [JFUserInfoManager logoutComplish:^(id result, NSError * error) {}];
            return ;
        }
        
        complish(responseObject, nil);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complish(nil, error);
        [MBProgressHUD hideLoading];//一定要写在前面，有的界面有loading，先停止loading再显示下面的文字
    }];
}



+ (void)uploadFileWithHostIp:(NSString *)hostIp
                   UrlString:(NSString *)URLString
                  parameters:(NSDictionary *)parameters
                       image:(UIImage *)image
                    videoUrl:(NSString *)videoUrl
                    complish:(void (^)(id responseObject, NSError *error))complish
{
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:parameters];
    
    //token
//    NSString *token = [YYKeychain getPasswordForService:kServiceName account:kAccountName];
//    if (!EmptyStr(token)) {
//        [params setObject:token forKey:@"token"];
//    }
    
    __block NSString *paramsStr = @"";
    [params enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        paramsStr = [paramsStr stringByAppendingFormat:@"&%@=%@", key, obj];
    }];
    NSLog(@"upload👉  %@%@?%@\n", hostIp, URLString, paramsStr);
    
    CBENetworkClass *netClass = [CBENetworkClass shared];
    [netClass setBaseUrl:hostIp];
    
    [netClass POST:URLString parameters:params headers:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        if (image) {
            //压缩质量，压缩不超过500K
            NSData *imageData = [image compressWithMaxLength:500.0f*1024.0f];
            NSString *mimeType = @"image/jpeg";
            if (!imageData) {
                imageData = UIImagePNGRepresentation(image);
                mimeType = @"image/png";
            }
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            formatter.dateFormat = @"yyyyMMddHHmmss";
            NSString *str = [formatter stringFromDate:[NSDate date]];
            NSString *fileName = [NSString stringWithFormat:@"%@.jpg", str];
            [formData appendPartWithFileData:imageData name:@"image" fileName:fileName mimeType:mimeType];
        }else{
            [formData appendPartWithFileURL:[NSURL fileURLWithPath:videoUrl] name:@"file" error:nil];
        }
    } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([StrFmt(@"%@",responseObject[@"recode"]) isEqualToString:@"1"]) {
            complish(responseObject, nil);
        }else{
            [MBProgressHUD hideLoading];
            [MBProgressHUD showMessageWithBottomText:responseObject[@"status"]];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        complish(nil, error);
        [MBProgressHUD hideLoading];
        [MBProgressHUD showMessageWithBottomText:@"网络不给力"];
    }];
}


+ (void)downloadFileWithfilePath:(NSString *)path
                          urlStr:(NSString *)urlStr
                        complish:(void (^)(id responseObject, NSError *error))complish {
    
}

@end
