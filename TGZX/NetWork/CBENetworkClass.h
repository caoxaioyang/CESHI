//
//  CBENetworkClass.h
//  CBECommerce
//
//  Created by jifan on 2020/5/19.
//  Copyright © 2020 Eleven_Liu. All rights reserved.
//

#import "AFHTTPSessionManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBENetworkClass : AFHTTPSessionManager

+ (instancetype)shared;

+ (BOOL)hasNetwork;

/** get请求 **/
+ (void)getWithHostIp:(NSString *)hostIp
            UrlString:(NSString *)URLString
              parameters:(NSDictionary *)parameters
                complish:(void (^)(id responseObject, NSError *error))complish;

/** post请求 **/
+ (void)postWithHostIp:(NSString *)hostIp
             UrlString:(NSString *)URLString
              parameters:(NSDictionary *)parameters
                complish:(void (^)(id responseObject, NSError *error))complish;

/** 上传 **/
+ (void)uploadFileWithHostIp:(NSString *)hostIp
                   UrlString:(NSString *)URLString
                  parameters:(NSDictionary *)parameters
                       image:(UIImage *)image
                    videoUrl:(NSString *)videoUrl
                    complish:(void (^)(id responseObject, NSError *error))complish;

/** 下载 **/
+ (void)downloadFileWithfilePath:(NSString *)path
                          urlStr:(NSString *)urlStr
                        complish:(void (^)(id responseObject, NSError *error))complish;

@end

NS_ASSUME_NONNULL_END
