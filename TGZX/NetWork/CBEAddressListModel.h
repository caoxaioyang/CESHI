//
//  CBEAddressListModel.h
//  CBECommerce
//
//  Created by jifan on 2020/5/19.
//  Copyright © 2020 Eleven_Liu. All rights reserved.
//

#import "CBEBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CBEAddressListDetailModel : CBEBaseModel
@property (nonatomic, strong) NSNumber *addressId;
@property (nonatomic, strong) NSNumber *user_id;
@property (nonatomic, copy  ) NSString *consignee_name;
@property (nonatomic, copy  ) NSString *phone_number;
@property (nonatomic, strong) NSNumber *province_id;
@property (nonatomic, strong) NSNumber *city_id;
@property (nonatomic, strong) NSNumber *area_id;
@property (nonatomic, copy  ) NSString *detailed_address;
@property (nonatomic, strong) NSNumber *is_default;
@property (nonatomic, strong) NSNumber *add_time;
@property (nonatomic, copy  ) NSString *province_name;
@property (nonatomic, copy  ) NSString *city_name;
@property (nonatomic, copy  ) NSString *area_name;
@end

@interface CBEAddressListModel : CBEBaseModel
@property (nonatomic, strong) NSArray *list;
@property (nonatomic, strong) NSNumber *count;
@end

NS_ASSUME_NONNULL_END
