//
//  CBEBaseModel.m
//  CBECommerce
//
//  Created by jifan on 2020/5/19.
//  Copyright © 2020 Eleven_Liu. All rights reserved.
//

#import "CBEBaseModel.h"

@implementation CBEBaseModel

MJCodingImplementation

+ (NSArray<NSString *> *)setupDataFieldsIsModelArray:(BOOL *)isModelArray {
    return nil;
}

+ (void)getWithHostIp:(NSString *)hostIp
            UrlString:(NSString *)URLString
              parameters:(NSDictionary *)parameters
                 isCache:(BOOL)isCache
                complish:(void (^)(id responseObject, NSString *errMsg, NSError *error))complish {
    
    [self requestMethod:AFRequestTypeGET HostIp:hostIp urlString:URLString parameters:parameters isCache:isCache complish:complish];
}

+ (void)postWithHostIp:(NSString *)hostIp
             UrlString:(NSString *)URLString
               parameters:(NSDictionary *)parameters
                  isCache:(BOOL)isCache
                 complish:(void (^)(id responseObject, NSString *errMsg, NSError *error))complish {
    
    [self requestMethod:AFRequestTypePOST HostIp:hostIp urlString:URLString parameters:parameters isCache:isCache complish:complish];
}


+ (void)requestMethod:(AFRequestType)type
               HostIp:(NSString *)hostIp
            urlString:(NSString *)URLString
           parameters:(NSDictionary *)parameters
              isCache:(BOOL)isCache
             complish:(void (^)(id responseObject, NSString *errMsg, NSError *error))complish{
    
//    YYCache *cache = [[YYCache alloc] initWithName:KYYRequestCache];
//    cache.memoryCache.shouldRemoveAllObjectsOnMemoryWarning = YES;
//    cache.memoryCache.shouldRemoveAllObjectsWhenEnteringBackground = YES;
    
//    id object = [cache objectForKey:URLString];
    
//    if (isCache) {
//        if (object) complish(object,@"cache",nil);
//    }
    
    //没有网络直接reture
    if (![CBENetworkClass hasNetwork]) {
        NSError *error = [NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorSecureConnectionFailed userInfo:nil]; //-1200
        complish(nil,nil,error);
        [MBProgressHUD showMessageWithBottomText:@"抱歉，请稍后重试"];
        return;
    }
    
    BOOL isModelArray = false;
    
    NSArray *fields = [self setupDataFieldsIsModelArray:&isModelArray];
    
    switch (type) {
            
        case AFRequestTypeGET:{
            [CBENetworkClass getWithHostIp:hostIp UrlString:URLString parameters:parameters complish:^(id responseObject, NSError *error) {
//                NSString *code = StrFmt(@"%@", responseObject[@"code"]);
                if (responseObject) {
                    for (NSInteger index = 0; index < fields.count; index++) responseObject = responseObject[fields[index]];
                    id result = nil;
                    
                    if (isModelArray) {
                        result = [[self class] mj_objectArrayWithKeyValuesArray:responseObject];   //耗时操作
                    }else {
                        result = [[self class] mj_objectWithKeyValues:responseObject];             //耗时操作
                    }
                    
                    complish(result,nil,nil);
//                    if (isCache) {
//                        [cache setObject:result forKey:URLString];
//                    }
                    
                }else {
                    complish(nil,responseObject[@"errMsg"],error);
                }
            }];
            break;
        }
            
        case AFRequestTypePOST:{
            
            [CBENetworkClass postWithHostIp:hostIp UrlString:URLString parameters:parameters complish:^(id responseObject, NSError *error) {
//                NSString *code = StrFmt(@"%@", responseObject[@"code"]);
                NSLog(@"responseObject=%@",responseObject);
                if (responseObject) {
                    for (NSInteger index = 0; index < fields.count; index++) responseObject = responseObject[fields[index]];
                    id result = nil;
                    
                    if (isModelArray) {
                        result = [[self class] mj_objectArrayWithKeyValuesArray:responseObject];   //耗时操作
                    }else {
                        result = [[self class] mj_objectWithKeyValues:responseObject];             //耗时操作
                    }
                    
                    complish(result,nil,nil);
//                    if (isCache) {
//                        [cache setObject:result forKey:URLString];
//                    }
                    
                }else {
                    complish(nil,responseObject[@"errMsg"],error);
                }
            }];
            
            break;
        }
        default:
            break;
    }
}

@end
