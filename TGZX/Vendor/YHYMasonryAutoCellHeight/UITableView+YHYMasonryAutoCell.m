//
//  UITableView+YHYMasonryAutoCell.m
//  YHYMasonryAutoCellHeight
//
//  Created by 断续quiet on 16/3/15.
//  Copyright © 2016年 断续quiet. All rights reserved.
//

#import "UITableView+YHYMasonryAutoCell.h"
#import <objc/runtime.h>
const void *__yhy_tableView_reuseCell = @"__yhy_tableView_reuseCellKey";
@implementation UITableView (YHYMasonryAutoCell)

- (NSMutableDictionary *)yhy_reuseCells{
    
    NSMutableDictionary *cells = objc_getAssociatedObject(self, __yhy_tableView_reuseCell);
    if (cells == nil) {
        cells = [[NSMutableDictionary alloc] init];
        objc_setAssociatedObject(self,
                                 __yhy_tableView_reuseCell,
                                 cells,
                                 OBJC_ASSOCIATION_RETAIN);
    }
    return cells;
}
@end
