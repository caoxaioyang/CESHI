//
//  UITableViewCell+YHYMasoneyCell.h
//  YHYMasonryAutoCellHeight
//
//  Created by 断续quiet on 16/3/16.
//  Copyright © 2016年 断续quiet. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MessageCell;
typedef void (^configBlock)(UITableViewCell *sourceCell);
@interface UITableViewCell (YHYMasoneyCell)
/**
 *  cell里面的最后一个View
 */
@property (nonatomic, strong) UIView *yhy_lastViewInCell;
/**
 *  距离Cell底部的偏移量
 */
@property (nonatomic, assign) CGFloat yhy_bottomOffsetToCell;
/**
 *  计算Cell的高度
 */
+(CGFloat)yhy_tableView:(UITableView *)tableView config:(configBlock)config;
@end
