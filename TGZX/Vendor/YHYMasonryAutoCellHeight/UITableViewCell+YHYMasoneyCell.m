//
//  UITableViewCell+YHYMasoneyCell.m
//  YHYMasonryAutoCellHeight
//
//  Created by 断续quiet on 16/3/16.
//  Copyright © 2016年 断续quiet. All rights reserved.
//

#import "UITableViewCell+YHYMasoneyCell.h"
#import <objc/runtime.h>
#import "UITableView+YHYMasonryAutoCell.h"
const void *s_yhy_bottomOffsetToCellKey = @"_yhy_bottomOffsetToCellKey";
const void *s_yhy_lastViewInCellKey = @"_yhy_lastViewCellKey";
@implementation UITableViewCell (YHYMasoneyCell)

+ (CGFloat)yhy_tableView:(UITableView *)tableView config:(configBlock)config{
    
    UITableViewCell *cell = [tableView.yhy_reuseCells objectForKey:[[self class] description]];

    if (cell == nil) {
        cell = [[self alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        [tableView.yhy_reuseCells setObject:cell forKey:[[self class] description]];
    }
    if (config) {
        config(cell);
    }
    return [cell private_yhy_heightForTableView:tableView];
}

- (CGFloat)private_yhy_heightForTableView:(UITableView *)tableView{
    
    [self layoutIfNeeded];
    CGFloat height = self.yhy_lastViewInCell.frame.size.height + self.yhy_lastViewInCell.frame.origin.y + self.yhy_bottomOffsetToCell;
    return height;
}


#pragma setting AND getting

- (void)setYhy_bottomOffsetToCell:(CGFloat)yhy_bottomOffsetToCell{
    objc_setAssociatedObject(self,
                             s_yhy_bottomOffsetToCellKey,
                             @(yhy_bottomOffsetToCell),
                             OBJC_ASSOCIATION_ASSIGN);
}

- (CGFloat)yhy_bottomOffsetToCell{
    NSNumber *objValue = objc_getAssociatedObject(self, s_yhy_bottomOffsetToCellKey);
    
    if ([objValue respondsToSelector:@selector(floatValue)]) {
        return objValue.floatValue;
    }
    return 0.0;
}

- (void)setYhy_lastViewInCell:(UIView *)yhy_lastViewInCell{
    objc_setAssociatedObject(self, s_yhy_lastViewInCellKey, yhy_lastViewInCell, OBJC_ASSOCIATION_RETAIN);
}

- (UIView *)yhy_lastViewInCell{
    return objc_getAssociatedObject(self, s_yhy_lastViewInCellKey);
}



@end
