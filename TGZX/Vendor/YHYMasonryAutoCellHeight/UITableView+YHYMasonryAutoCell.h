//
//  UITableView+YHYMasonryAutoCell.h
//  YHYMasonryAutoCellHeight
//
//  Created by 断续quiet on 16/3/15.
//  Copyright © 2016年 断续quiet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (YHYMasonryAutoCell)
/**
 *  用于获取Cell的行高,只需要一个就够了
 */
@property (nonatomic, strong, readonly) NSMutableDictionary *yhy_reuseCells;
@end
